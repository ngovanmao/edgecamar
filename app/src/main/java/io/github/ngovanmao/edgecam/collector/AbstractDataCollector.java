package io.github.ngovanmao.edgecam.collector;

public abstract class AbstractDataCollector {
    protected String mSensorName;

    public String getSensorName() {
        return mSensorName;
    }

    public abstract void start();
    public abstract void stop();
    public abstract void haltAndRestartLogging();

}
