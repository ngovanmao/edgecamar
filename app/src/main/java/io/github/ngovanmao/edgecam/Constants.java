package io.github.ngovanmao.edgecam;

public final class Constants {
    private Constants() {
    }
    public static final String PACKAGE_NAME = "com.example.android.datafrominternet";
    public static final String SENSOR_NAME_ACC = "Accelerometer";
    public static final String SENSOR_NAME_GYR = "Gyroscope";
    public static final String SENSOR_NAME_MAG = "Magnetometer";
    public static final String SENSOR_NAME_WIFI = "WiFi";
    public static final String SENSOR_NAME_LOC = "Location";
    public static final String COMMAND_SERVICE_INTENT_KEY = PACKAGE_NAME + ".COMMAND_SERVICE_INTENT_KEY";

    /**
     * Defines a shared preference that stores the data collection object. It's empty when
     * the data collection is not running. It is stored persistently in case the device is rebooted.
     */
    public static final String DATA_COLLECTION_SESSION_OBJECT_KEY = PACKAGE_NAME + ".DATA_COLLECTION_SESSION_OBJECT_KEY";

    public static final int UI_ERROR_CODE_EXTERNAL_STORAGE = 0;
    public static final int UI_ERROR_CODE_SENSORS_DISABLED = 1;
    public static final int UI_ERROR_CODE_INTERNET_CONNECTIVITY = 2;
    public static final int UI_ERROR_CODE_FILE_UPLOAD = 3;
    public static final int UI_ERROR_CODE_NO_UNSYNC_FILES = 4;
    public static final int UI_ERROR_CODE_COLLECTORS_START = 5;

    public static final int IMAGE_INPUT_SIZE = 416;

    /*
    Each source has its own standard, and each WiFi chip has its own range.
    https://www.netspotapp.com/what-is-rssi-level.html
    Excellent            > -50 dBm
    High quality (90%)   ~= -55 dBm
    Medium quality (50%) ~= -75 dBm
    Low quality (30%)    ~= -85 dBm
    Unusable quality (8%)~= -96 dBm
    https://eyesaas.com/wi-fi-signal-strength/
    I choose the following number
    */
    public static final int RSSI_EXCELLENT = -50;
    public static final int RSSI_HIGH   = -60;
    public static final int RSSI_MEDIUM = -67;
    public static final int RSSI_LOW    = -70;
    public static final int RSSI_UNRELIABLE = -80;
    public static final int RSSI_UNUSABLE = -90;

    /*
    MQTT topics:
     */
    public static final int BROKER_PORT = 9999;
    // Publishing topic
    public static final String MQTT_DISCOVER = "discover";
    public static final String MQTT_MONITOR = "monitor/eu/";
    public static final String MQTT_MONITOR_SERVICE = "monitor/service/";
    // subscribed topic
    public static final String MQTT_MIGRATED = "migrated/";
    public static final String MQTT_HANDOVER = "handover/";
    public static final String MQTT_ALLOCATED = "allocated/";


    public static final String CHANGE_RUNNING_SERVICE_ACTION = "CHANGE_RUNNING_SERVICE";
    public static final String CHANGE_MONITOR_SERVICE_ACTION = "CHANGE_MONITOR_SERVICE";;
    public static final String MONITOR_NETWORK_SERVICE_ACTION = "MONITOR_NETWORK_SERVICE_ACTION";
    public static final String MONITOR_SLA_ACTION = "MONITOR_SLA_ACTION";
    public static final String BROADCAST_MQTT_HANDOVER_ACTION = "BROADCAST_MQTT_HANDOVER_ACTION";
}
