package io.github.ngovanmao.edgecam.sync;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

public class EdgeSyncIntentService extends IntentService{
    public EdgeSyncIntentService() {
        super("EdgeSyncIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        EdgeSyncTask.syncEdgeServer(this);
    }
}
