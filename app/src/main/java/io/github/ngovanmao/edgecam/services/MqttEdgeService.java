package io.github.ngovanmao.edgecam.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import io.github.ngovanmao.edgecam.Constants;
import io.github.ngovanmao.edgecam.R;
import io.github.ngovanmao.edgecam.SharedPreferencesHelper;

public class MqttEdgeService extends Service {
    private final static String TAG = MqttEdgeService.class.getSimpleName();

    private MqttAndroidClient mqttAndroidClient;
    private String mMonitor;
    private String mAllocated;
    private String mMigrated;
    private String mHandover;
    private String mMonitorService;
    private Context mContext;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "called onStartCommand");
        mContext = getApplicationContext();
        String brokerIP = SharedPreferencesHelper.getBrokerIP(mContext);
        String userName = "User" +
                Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        SharedPreferencesHelper.setUserName(mContext, userName);
        String serverUri = "tcp://" + brokerIP + ":" + String.valueOf(Constants.BROKER_PORT);
        mAllocated = Constants.MQTT_ALLOCATED + userName;
        mMigrated = Constants.MQTT_MIGRATED + userName;
        mHandover = Constants.MQTT_HANDOVER + userName;
        mMonitor = Constants.MQTT_MONITOR + userName;
        mMonitorService = Constants.MQTT_MONITOR_SERVICE + userName;
        mqttAndroidClient = new MqttAndroidClient(mContext, serverUri, userName);

        // Set call back for handling receiving mqtt message of subscribed topics.
        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {
                Log.w(TAG, serverURI);
            }

            @Override
            public void connectionLost(Throwable cause) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                Log.w(TAG, "Arrvied topic " + topic + " Message " + message.toString());
                if (topic.equals(mAllocated)) {
                    updateServiceInfo(message.toString());
                }
                if (topic.equals(mMigrated)) {
                    updateServiceInfo(message.toString());
                }
                if (topic.equals(mHandover)) {
                    broadcastHandoverMsg(message.toString());
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });
        connect();

        //Register local broadcast manager for Mqtt messages
        IntentFilter receivingIntents = new IntentFilter(Constants.MONITOR_NETWORK_SERVICE_ACTION);
        receivingIntents.addAction(Constants.CHANGE_RUNNING_SERVICE_ACTION);
        receivingIntents.addAction(Constants.MONITOR_SLA_ACTION);
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mMessageReceiver, receivingIntents);
        return START_STICKY;
    }

    private void updateServiceInfo(String message) {
        try {
            JSONObject updatedService = new JSONObject(message);
            String serviceName = updatedService.getString("service_name");
            String serviceIP = updatedService.getString("ip");
            String servicePort = updatedService.getString("port");
            Log.d(TAG, String.format("Update service %s runs at server IP:port %s : %s",
                    serviceName, serviceIP, servicePort));
            SharedPreferencesHelper.setServiceInfo(mContext, serviceName, serviceIP, servicePort);
            //SharedPreferencesHelper.setEnabledServiceInfo(mContext, nextEdgeIP, nextEdgePort);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void connect() {
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(false);
        String lwtTopic = "LWT/eu/"+SharedPreferencesHelper.getUserName(mContext);
        String lwtPayload = "Unexpected exit";
        mqttConnectOptions.setWill(lwtTopic, lwtPayload.getBytes(), 1, false);
        Log.d(TAG, "connect");
        try {
            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.d(TAG, "MQTT connect success.");
                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
                    disconnectedBufferOptions.setBufferEnabled(true);
                    disconnectedBufferOptions.setBufferSize(100);
                    disconnectedBufferOptions.setPersistBuffer(false);
                    disconnectedBufferOptions.setDeleteOldestMessages(false);
                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);
                    subscribeToTopic();
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.w(TAG, "Failed to connect to "  + exception.getMessage());

                }
            });
        } catch (MqttException ex) {
            ex.printStackTrace();
        }
    }

    private void subscribeToTopic() {
        try {
            mqttAndroidClient.subscribe(mAllocated, 1, null,
                    new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.w("Mqtt","Subscribed allocated!");
                    if (SharedPreferencesHelper.isEnabledServiceYolo(mContext)) {
                        discoveryService(getString(R.string.pref_service_yolo_key));
                    } else if (SharedPreferencesHelper.isEnabledServiceOpenface(mContext)) {
                        discoveryService(getString(R.string.pref_service_openface_key));
                    }
                }
                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.w("Mqtt", "Subscribed allocated fail!");
                }
            });
            mqttAndroidClient.subscribe(mHandover, 1, null,
                new IMqttActionListener() {
                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        Log.w("Mqtt","Subscribed handover!");
                    }
                    @Override
                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                        Log.w("Mqtt", "Subscribed handover fail!");
                    }
            });
            mqttAndroidClient.subscribe(mMigrated, 1, null,
                new IMqttActionListener() {
                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        Log.w("Mqtt","Subscribed migrated!");
                    }
                    @Override
                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                        Log.w("Mqtt", "Subscribed migrated fail!");
                    }
            });
        } catch (MqttException ex) {
            System.err.println("Exceptionst subscribing");
            ex.printStackTrace();
        }
    }

    private void discoveryService(String serviceName) {
        String userName = SharedPreferencesHelper.getUserName(mContext);
        String[] ssidInfo = SharedPreferencesHelper.getApInfo(mContext).split("\\s+");
        if (serviceName != null) {
            String discoveryMsg = String.format("{\"service_name\": %s,\"end_user\": %s," +
                            "\"ssid\": %s,\"bssid\": \"%s\"}",
                    serviceName, userName, ssidInfo[0], ssidInfo[1]);
            myPublish(Constants.MQTT_DISCOVER, discoveryMsg, 1);
        }
    }

    // Handling the received Intents for publish MQTT message
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            if (intent == null || !mqttAndroidClient.isConnected()) {
                return;
            }
            if (Constants.MONITOR_NETWORK_SERVICE_ACTION.equals(intent.getAction())) {
                String monitorMessage = intent.getStringExtra(Constants.MONITOR_NETWORK_SERVICE_ACTION);
                myPublish(mMonitor, monitorMessage, 0);
            }
            if (Constants.CHANGE_RUNNING_SERVICE_ACTION.equals(intent.getAction())){
                Log.d(TAG, "receive localbroadcast change running service");
                String serviceName = intent.getStringExtra(Constants.CHANGE_RUNNING_SERVICE_ACTION);
                discoveryService(serviceName);
            }
            if (Constants.MONITOR_SLA_ACTION.equals(intent.getAction())) {
                String slaMessage = intent.getStringExtra(Constants.MONITOR_SLA_ACTION);
                //Log.d(TAG, "receive localbroadcast " + slaMessage);
                myPublish(mMonitorService, slaMessage, 0);
            }
        }
    };

    private void broadcastHandoverMsg(String message) {
        Intent intent = new Intent(Constants.BROADCAST_MQTT_HANDOVER_ACTION);
        intent.putExtra(Constants.BROADCAST_MQTT_HANDOVER_ACTION, message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void myPublish(String topic, String payload, int qos) {
        try {
            MqttMessage m = new MqttMessage();
            m.setPayload(payload.getBytes());
            m.setQos(qos);
            m.setRetained(false);
            mqttAndroidClient.publish(topic, m);
            Log.d(TAG, "sent a message" + m.toString());
        } catch (MqttException ex) {
            ex.printStackTrace();
            Log.e(TAG, String.format("error publishing topic:%s, msg: %s, error: %s",
                    topic, payload, ex.getMessage()));
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mqttAndroidClient.isConnected()) {
            mqttAndroidClient.unregisterResources();
            mqttAndroidClient.close();
        }
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(mMessageReceiver);
        Log.d(TAG, "Destroy Mqtt edge service");
    }

}
