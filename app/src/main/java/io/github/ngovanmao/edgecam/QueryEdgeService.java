package io.github.ngovanmao.edgecam;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import io.github.ngovanmao.edgecam.collector.NetPerfDataCollector;
import io.github.ngovanmao.edgecam.log.DataCollectionSession;
import io.github.ngovanmao.edgecam.utilities.ParserXmlResults;
import io.github.ngovanmao.edgecam.utilities.TcpClient;

import java.io.IOException;
import java.util.Calendar;


public class QueryEdgeService extends Service {
    private static final String TAG = QueryEdgeService.class.getSimpleName();
    private NetPerfDataCollector mNetPerfDataCollector = null;
    private DataCollectionSession mDataCollectionSession = null;
    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());

    /** Command to the service to display a message */
    static final int MSG_SEND_IMAGE_PATH = 1;
    static final int MSG_SAY_HELLO = 100;

    static final String OPENFACE_RESULT_KEY = "RecognitionResults";
    static final String OPENFACE_RETURN_ACTION = "OpenfaceReturn";
    private JSONObject mUserInfo = new JSONObject();
    private Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        if (mDataCollectionSession == null || mNetPerfDataCollector == null) {
            try {
                // A new data collection session object is created
                mDataCollectionSession = new DataCollectionSession(
                        Calendar.getInstance().getTime(),
                        getResources().getString(R.string.session_id_date_dateformat),
                        SharedPreferencesHelper.getUserName(mContext),
                        System.currentTimeMillis());
                // The session id is generated for the new data collection session
                String sessionName = mDataCollectionSession.getSessionName();
                Log.i(TAG, "::QueryEdgeService starts data collection with session name: " + sessionName);

                try {
                    mNetPerfDataCollector = new NetPerfDataCollector(mContext, sessionName,
                            "NetPerf", SharedPreferencesHelper.getLogFilesMaxsize(mContext));
                    mNetPerfDataCollector.start();
                } catch (Exception e) {
                    Log.e(TAG, "Error starting NetPerf data collector" + e.getMessage());
                }
                // If there were no errors starting data collection, the data collection object is stored.
                SharedPreferencesHelper.setDataCollectionSessionObject(this, mDataCollectionSession);

            } catch (Exception e) {
                Log.e(TAG, "::onStartCommand Error processing message : " + e.getMessage());
            }
        }
        String serviceName = SharedPreferencesHelper.getEnabledServiceName(mContext);
        String userName = SharedPreferencesHelper.getUserName(mContext);
        String[] ssidInfo = SharedPreferencesHelper.getApInfo(mContext).split("\\s+");
        try {
            mUserInfo.put("end_user", userName);
            mUserInfo.put("service_name", serviceName);
            mUserInfo.put("ssid", ssidInfo[0]);
            mUserInfo.put("bssid", ssidInfo[0]);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handler of incoming messages from clients.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SAY_HELLO:
                    Toast.makeText(getApplicationContext(), "hello!", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "received message " + msg.toString());
                    break;
                case MSG_SEND_IMAGE_PATH:
                    String messages = msg.obj.toString();
                    String[] splitMessage = messages.split(":");
                    makeQueryEdge(splitMessage);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }


    /**
     * When binding to the service, we return an interface to our messenger
     * for sending messages to the service.
     */
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Binding service");
        return mMessenger.getBinder();
    }


    private void makeQueryEdge(String[] messages) {
        new MultipleQueriesEdgeTask().execute(messages);
    }

    public class MultipleQueriesEdgeTask extends AsyncTask<String, String, String> {
        TcpClient edgeTcpClient = null;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String serverIP = strings[0];
            String serverPort = strings[1];
            String imagePath = strings[2];
            int numberQueries = Integer.parseInt(strings[3]);
            String results = null;
            int runningIdx = 0;

            if (imagePath == null || imagePath.equals("") || imagePath.equals("null")) {
                return null;
            }
            try {
                edgeTcpClient = new TcpClient(serverIP, Integer.valueOf(serverPort), mContext);
            } catch (IOException e) {
                e.printStackTrace();
                results = "LostConnection";
                return results;
            }
            while (runningIdx < numberQueries) {
                try {
                    results =  edgeTcpClient.sendTcpScaleImage(imagePath);
                } catch (IOException e) {
                    e.printStackTrace();
                    results = "LostConnection";
                    return results;
                }
                publishProgress(results);
                runningIdx += 1;
            }
            edgeTcpClient.forceCloseTcpClient();
            return null; // just return no need to do anything.
        }

        @Override
        protected void onProgressUpdate(String[] returnMsgs) {
            handleReturnMsg(returnMsgs[0]);
        }

        @Override
        protected void onPostExecute(String returnMsg) {
            handleReturnMsg(returnMsg);
        }

        private void handleReturnMsg(String returnMsg) {
            if (returnMsg != null && !returnMsg.equals("")) {
                sendResultsToMainActivity(returnMsg);
                if (!returnMsg.equals("LostConnection")) {
                    if (SharedPreferencesHelper.isEnabledMigration(mContext)) {
                        broadcastMonitorMessage(returnMsg);
                    }
                    mNetPerfDataCollector.writeLog(returnMsg);
                    mNetPerfDataCollector.writeLog(System.lineSeparator());
                }
            }
        }
    }

    private void broadcastMonitorMessage(String message) {
        String serviceInfoMsg = null;
        try {
            serviceInfoMsg = ParserXmlResults.getServiceMonitoringInfo(message, mUserInfo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (serviceInfoMsg != null) {
            Log.d(TAG, "Sending service status " + serviceInfoMsg);
            Intent intent = new Intent(Constants.MONITOR_SLA_ACTION);
            intent.putExtra(Constants.MONITOR_SLA_ACTION, serviceInfoMsg);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
        /*
        // Get only partial message info
        // {'startTime[ns]':3181611284214279,'endTime[ns]':3181611693577664,
        // 'allResults':{'originSize[B]':2137960,'sentSize[B]':5765
        // and add with user information.
        String sentMsg = message.split(",\'scaleFactor")[0] + mUserInfo;
        Intent intent = new Intent(Constants.MONITOR_SLA_ACTION);
        intent.putExtra(Constants.MONITOR_SLA_ACTION, sentMsg);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);*/
    }

    private void sendResultsToMainActivity(String detectedResults) {
        // The string OPENFACE_RETURN_ACTION will be used to filer the intent
        Intent intent = new Intent(OPENFACE_RETURN_ACTION);
        intent.putExtra(OPENFACE_RESULT_KEY, detectedResults);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        // TODO: need to carefully comment out again.
        // mNetPerfDataCollector.stop();
        Log.d(TAG, "onDestroy method");
    }
}
