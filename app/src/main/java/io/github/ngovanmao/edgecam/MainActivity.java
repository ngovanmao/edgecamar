/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.github.ngovanmao.edgecam;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import io.github.ngovanmao.edgecam.services.MonitorNetwork;
import io.github.ngovanmao.edgecam.services.MqttEdgeService;
import io.github.ngovanmao.edgecam.view.BoxAndImageView;
import io.github.ngovanmao.edgecam.utilities.ParserXmlResults;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int PERMISSIONS_REQUEST = 1;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final int REQUEST_IMAGE_CAPTURE = 2;
    private static final String[] PERMISSIONS = {Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
    };

    private BoxAndImageView mImageView;
    private Bitmap mPhotoBitmap;
    private String picturePath;
    /** Messenger for communicating with the service. */
    Messenger mService = null;
    /** Flag indicating whether we have called bind on the service. */
    boolean mBound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        mImageView = (BoxAndImageView) findViewById(R.id.Imageprev);

        requestPermission();

        if (SharedPreferencesHelper.isEnableLogFiles(MainActivity.this)) {
            Log.d(TAG, "isEnableLogFiles and start service");
            Intent dataCollectionService = new Intent(MainActivity.this, DataCollectionService.class);
            startService(dataCollectionService);
        }
        // Monitoring network service
        Intent monitoringService = new Intent(MainActivity.this, MonitorNetwork.class);
        startService(monitoringService);

        Intent mqttService = new Intent(MainActivity.this, MqttEdgeService.class);
        startService(mqttService);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to the service
        Intent queryEdgeServiceIntent = new Intent(this, QueryEdgeService.class);
        bindService(queryEdgeServiceIntent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    private boolean checkIfAlreadyhavePermission(Context context, String[] permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    private void requestPermission(){
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSIONS_REQUEST);
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSIONS_REQUEST:
                if (permissions.length >0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED
                        && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                    break;
                } else {
                    requestPermission();
                }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        //File storageDir = getExternalStoragePublicDirectory(DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        picturePath = image.getAbsolutePath();
        return image;
    }

    Uri mPhotoUri;
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // create the file where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                mPhotoUri = FileProvider.getUriForFile(this,
                        getString(R.string.app_authority),
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Select image from button
        if (requestCode == REQUEST_CAMERA_PERMISSION && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = data.getData();
            if (data.getExtras() != null) {
                mPhotoBitmap = (Bitmap) data.getExtras().get("data");
            } else {
                Log.e(TAG, "cannot get extras from data");
            }

            picturePath = getPicturePath(selectedImage);
            Log.d(TAG, "image path is " + picturePath);

            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
            mImageView.setMyImageBitmap(bitmap);
            Toast.makeText(this, "image lala", Toast.LENGTH_LONG).show();
        }
        // Capture image from camera device
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            mPhotoBitmap = BitmapFactory.decodeFile(picturePath);
            mImageView.setMyImageBitmap(mPhotoBitmap);
        }
    }


    private String getPicturePath(Uri imagePathUri){
        // Cursor to get image uri to display
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(imagePathUri,
                filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String imagePath = cursor.getString(columnIndex);
        cursor.close();
        return imagePath;
    }

    /*private Bitmap getScaleBitmap(Bitmap bitmap) {
        int currentBitmapWidth = bitmap.getWidth();
        int currentBitmapHeight = bitmap.getHeight();
        int ivWidth = mImageView.getWidth();
        int ivHeight = mImageView.getHeight();

        int newWidth= ivWidth;
        int newHeight = (int) Math.floor((double) currentBitmapHeight *
                ((double) newWidth/(double)currentBitmapWidth));
        return Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        // This registers mMessageReceiver to receive messages.
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mMessageReceiver,
                        new IntentFilter(QueryEdgeService.OPENFACE_RETURN_ACTION));
    }

    @Override
    protected void onPause() {
        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(mMessageReceiver);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, MonitorNetwork.class));
        stopService(new Intent(this, MqttEdgeService.class));

    }

    private void choosePicture() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CAMERA_PERMISSION);
    }


    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
            mService = new Messenger(service);
            mBound = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mService = null;
            mBound = false;
        }
    };

    // Handling the received Intents for the edge query service event
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            String detectedResults = intent.getStringExtra(QueryEdgeService.OPENFACE_RESULT_KEY);
            if (detectedResults.equals("LostConnection")) {
                Toast.makeText(MainActivity.this, "Lost connection to edge servers. Please check setting!", Toast.LENGTH_LONG).show();
                return;
            }
            List<Classifier.Recognition> results = null;
            try {
                //results = ParserXmlResults.getRecognitionResultsFromJson(detectedResults);
                results = ParserXmlResults.getRecognitionCombinedResultsFromJson(detectedResults);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mImageView.setResults(results);

        }
    };

    public void requestEdgeService() {
        if (!mBound) return;
        if (picturePath == null) {
            Toast.makeText(this, "Null Image. Please select image before query!", Toast.LENGTH_LONG).show();
            return;
        }

        String serviceInfo = SharedPreferencesHelper.getEnabledServiceInfo(this);
        if (serviceInfo == null || serviceInfo == "") {
            Log.e(TAG, "No service is discovered");
            Toast.makeText(this, "Failed to discover edge service. Please check setting!", Toast.LENGTH_LONG).show();
            return;
        }
        String serverIP = serviceInfo.split(":")[0];
        String serverPort = serviceInfo.split(":")[1];
        int numberQueries = SharedPreferencesHelper.getNumberContinuousQueries(this);
        String message = String.format("%s:%s:%s:%d", serverIP, serverPort, picturePath, numberQueries);
        Message setServerIPMsg = Message.obtain(null, QueryEdgeService.MSG_SEND_IMAGE_PATH,
                message);
        try {
            mService.send(setServerIPMsg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        //Toast.makeText(this,"Request edge service", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemThatWasClickedId = item.getItemId();
        switch (itemThatWasClickedId) {
            case R.id.action_settings:
                Intent settingIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingIntent);
                return true;
            case R.id.edge_query:
                requestEdgeService();
                return true;
            case R.id.action_camera:
                dispatchTakePictureIntent();
                return true;
            case R.id.action_select_image:
                choosePicture();
                return true;
            case R.id.action_realtime_camera:
                startRealTimeCamera();
                return true;
            case R.id.action_about:
                Intent showAboutInfo = new Intent(this, AboutActivity.class);
                startActivity(showAboutInfo);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startRealTimeCamera() {
        Intent realtimeCameraIntent = new Intent(this, CameraActivity.class);
        startActivity(realtimeCameraIntent);
    }

}
