package io.github.ngovanmao.edgecam.collector;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import io.github.ngovanmao.edgecam.log.CustomLogger;

import java.io.File;
import java.util.Date;

public class LocationDataCollector extends AbstractDataCollector
        implements LocationListener {
    private static final String TAG = LocationDataCollector.class.getSimpleName();

    private CustomLogger logger = null;

    private LocationManager mLocationManager = null;
    public LocationDataCollector(Context context, String sessionName, String sensorName, int logFileMaxSize){

        mSensorName = sensorName;
        String path = sessionName + File.separator + mSensorName + "_" + sessionName;

        logger = new CustomLogger(context, path, sessionName, mSensorName, "txt", false, logFileMaxSize);

        mLocationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location == null) return;

        // Return the time of this fix, in elapsed real-time since system boot.
        long locationNanoTime = location.getElapsedRealtimeNanos();

        // System local time in millis
        long currentMillis = (new Date()).getTime();

        // Get the estimated accuracy of this location, in meters.
        float accuracy = location.getAccuracy();

        // Get the latitude, in degrees.
        double latitude = location.getLatitude();

        // Get the longitude, in degrees.
        double longitude = location.getLongitude();

        // Get the altitude if available, in meters above the WGS 84 reference ellipsoid.
        double altitude = location.getAltitude();

        String message = String.format("%s", currentMillis) + ";"
                + String.format("%s", locationNanoTime) + ";"
                + accuracy + ";"
                + latitude + ";"
                + longitude + ";"
                + altitude;

//        // Get the latitude, in degrees.
//        String latitudeDegrees = Location.convert(latitude, Location.FORMAT_DEGREES);
//        // Get the longitude, in degrees.
//        String longitudeDegrees = Location.convert(longitude, Location.FORMAT_DEGREES);

        logger.log(message);
        logger.log(System.lineSeparator());
    }

    @Override
    public void onStatusChanged(String s, int status, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void start() {
        Log.i(TAG, "start:: Starting listener for sensor: " + getSensorName());
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, this);
        logger.start();
    }

    @Override
    public void stop() {
        Log.i(TAG,"stop:: Stopping listener for sensor " + getSensorName());
        logger.stop();
        if (mLocationManager != null) {
            mLocationManager.removeUpdates(this);
        }
    }

    @Override
    public void haltAndRestartLogging() {
        logger.stop();
        logger.resetByteCounter();
        logger.start();
    }
}
