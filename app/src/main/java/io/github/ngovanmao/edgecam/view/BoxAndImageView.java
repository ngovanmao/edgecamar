package io.github.ngovanmao.edgecam.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.List;

import io.github.ngovanmao.edgecam.Classifier.Recognition;
import io.github.ngovanmao.edgecam.R;

/*
 * This file is a work around solution for main activity without Texture view and want to display
 * image with bounding box.
 */
public class BoxAndImageView extends View {
    private static final String TAG = BoxAndImageView.class.getSimpleName();
    private List<Recognition> results;

    private Context mContext;
    private Paint mImagePaint;
    private Bitmap mDisplayImage;
    private RectF mBoxImageViewRect;

    private Paint mBoundingBoxPaint;
    private Paint mTextPaint;

    private RectF mBBRect;
    private float mBBX;
    private float mBBY;
    private float mBBWidth;
    private float mBBHeight;

    private float mTextX;
    private float mTextY;
    private float mTextHeight;
    private int mTextColor = Color.BLUE;
    private String mLabelString = "";

    /**
     * Class constructor taking only a context. Use this constructor to create
     * {@link BoxAndImageView} objects from your own code.
     *
     * @param context
     */
    public BoxAndImageView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    /**
     * Class constructor taking a context and an attribute set. This constructor
     * is used by the layout engine to construct a {@link BoxAndImageView} from a set of
     * XML attributes.
     *
     * @param context
     * @param attrs   An attribute set which can contain attributes from
     *                {@link R.styleable.BoxImageView} as well as attributes inherited
     *                from {@link View}.
     */
    public BoxAndImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;

        // attrs contains the raw values for the XML attributes that were specified in the layout,
        // which don't include attributes set by styles or themes, and which may have
        // unresolved references. Call obtainStyledAttributes() to get the final values for each attribute.
        //
        // This call uses R.styleable.PieChart, which is an array of
        // the custom attributes that were declared in attrs.xml.
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.BoxImageView,
                0, 0
        );

        try {
            // The R.styleable.BoxImageView_* constants represent the index for
            // each custom attribute in the R.styleable.BoxImageView array.
            mTextX = a.getDimension(R.styleable.BoxImageView_labelX, 100.0f);
            mTextY = a.getDimension(R.styleable.BoxImageView_labelY, 100.0f);
            mTextHeight = a.getDimension(R.styleable.BoxImageView_labelHeight, 30.0f);
            mTextColor = a.getColor(R.styleable.BoxImageView_labelColor, Color.MAGENTA);

            mBBX = a.getDimension(R.styleable.BoxImageView_boundingboxX, 0.0f);
            mBBY = a.getDimension(R.styleable.BoxImageView_boundingboxY, 0.0f);
            mBBWidth = a.getDimension(R.styleable.BoxImageView_boundingboxWidth, 0.0f);
            mBBHeight = a.getDimension(R.styleable.BoxImageView_boundingboxHeight, 0.0f);
        } finally {
            // release the TypedArray so that it can be reused.
            a.recycle();
        }
        init();
    }

    public void setResults(final List<Recognition> results) {
        this.results = results;
        postInvalidate();
    }

    public void setMyImageBitmap(final Bitmap capturedImage) {
        this.mDisplayImage = capturedImage;
        this.results = null;
        postInvalidate();
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        // Get view size.
        float view_height = this.getHeight();
        float view_width = this.getWidth();
        //Log.v(TAG, String.format("canvas width: %f, canvas height: %f", view_width, view_height));
        float imageW = mDisplayImage.getWidth();
        float imageH = mDisplayImage.getHeight();
        //Log.d(TAG, "imageW = " + Float.toString(imageW) + "imageH = " + Float.toString(imageH));
        float newWidth, newHeight;
        float scaleFactorW = view_width/imageW;
        float scaleFactorH = view_height/imageH;
        float scaleFactor;
        if (scaleFactorW < scaleFactorH) {
            scaleFactor = scaleFactorW;
            newWidth = view_width;
            newHeight = (int) Math.floor((double) imageH * scaleFactor);
        } else {
            scaleFactor = scaleFactorH;
            newHeight = view_height;
            newWidth = (int) Math.floor((double) imageW * scaleFactor);
        }

        //Log.d(TAG, String.format("newWidth=%f, newHeight=%f, scaleFactor=%f",newWidth, newHeight, scaleFactor));
        //int sensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
        mBoxImageViewRect = new RectF(0,0, newWidth, newHeight);
        canvas.drawBitmap(mDisplayImage, null, mBoxImageViewRect, mImagePaint);

        if (results != null) {
            for (final Recognition recog : results) {
                // Get x, y, width and height before pre processing of
                // bounding boxes. Then pre-process the bounding boxes
                // by using the multipliers and offsets to map a 448x448 image
                // coordinates to a device_width x device_height surface
                RectF preBoundingBox = recog.getLocation();
                boolean isPrediction = recog.getId().contains("Prediction");
                float bbLeft = (float) (preBoundingBox.left * scaleFactor);
                float bbTop = (float) (preBoundingBox.top * scaleFactor);
                float bbRight = (float) (preBoundingBox.right* scaleFactor);
                float bbBottom = (float) (preBoundingBox.bottom * scaleFactor);

                // Create new bounding box and draw it.
                mBBRect = new RectF(bbLeft, bbTop, bbRight, bbBottom);
                canvas.drawRect(mBBRect, mBoundingBoxPaint);


                // Create class name text on bounding box.
                if (isPrediction) {
                    mLabelString = "P : " + recog.getTitle();
                } else { //if (recog.getId().equals("Index"))
                    mLabelString = "ID:" + recog.getTitle();
                }
                mTextX = bbLeft;
                mTextY = bbTop - 3.0f;
                canvas.drawText(mLabelString, mTextX , mTextY, mTextPaint);
                if (isPrediction) {
                    mLabelString = "C : " + String.valueOf(recog.getConfidence());
                } else { //if (recog.getId().equals("Index"))
                    mLabelString = "ProcT:" + String.valueOf(recog.getConfidence());
                }
                mTextX = bbLeft;
                mTextY = bbBottom + 3.0f + mTextHeight;
                canvas.drawText(mLabelString, mTextX , mTextY, mTextPaint);
            }
        }
    }


    /**
     * Initialize the control. This code is in a separate method so that it can be
     * called from both constructors.
     */
    private void init() {

        // Set up the paint for image bitmap
        mImagePaint = new Paint();
        mDisplayImage = BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.system_arch);

        // Set up the Paint for bounding box
        mBoundingBoxPaint = new Paint();
        mBoundingBoxPaint.setColor(Color.RED);
        mBoundingBoxPaint.setStyle(Paint.Style.STROKE);
        mBoundingBoxPaint.setStrokeWidth(6);
        mBBRect = new RectF(mBBX, mBBY, mBBX + mBBWidth, mBBY + mBBHeight);

        // Set up the paint for the label text
        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setColor(mTextColor);
        if (mTextHeight == 0) {
            mTextHeight = mTextPaint.getTextSize();
        } else {
            mTextPaint.setTextSize(mTextHeight);
        }
        mTextPaint.setTextAlign(Paint.Align.LEFT);
    }
}
