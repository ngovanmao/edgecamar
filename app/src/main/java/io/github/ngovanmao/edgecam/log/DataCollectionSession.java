package io.github.ngovanmao.edgecam.log;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * Created by fjordonez on 01/10/16.
 * Modified by Mao Ngo, 20/04/2018.
 */
public class DataCollectionSession {
    private Date mStartDate;
    private Date mEndDate;
    private String mDateFormat;
    private String mUserName;
    private long mMillisSignature;
    private long mLength;
    private static String dateFormatISO8601 = "yyyy-MM-dd HH:mm:ss.SSS";

    public DataCollectionSession(Date startDate, String dateFormat, String userName,
                                 long millisSignature){
        mStartDate = startDate;
        mEndDate = null;
        mDateFormat = dateFormat;
        mUserName = userName;
        mMillisSignature = millisSignature;
        mLength = 0;
    }

    public String getUserName(){
        return mUserName;
    }

    public Date getStartDate(){
        return mStartDate;
    }

    public Date getEndDate(){
        return mEndDate;
    }

    public long getMillisSignature(){
        return mMillisSignature;
    }

    public long getLength(){
        return mLength;
    }


    public void setEndDate(Date endDate){
        mEndDate = endDate;
        setLength((mEndDate.getTime()-mStartDate.getTime())/1000);
    }

    public void setLength(long length){
        mLength = length;
    }

    public String getSessionId(){
        return new SimpleDateFormat(mDateFormat).format(getStartDate())
                + String.valueOf(getMillisSignature() % 100000);
    }

    public String getSessionName(){
        String ret = getUserName() + "_"  + getSessionId();
        return ret;
    }

    public String getStartDateISO8601(){
        return new SimpleDateFormat(dateFormatISO8601).format(getStartDate());
    }

    public String getEndDateISO8601(){
        if (getEndDate() != null){
            return new SimpleDateFormat(dateFormatISO8601).format(getEndDate());
        }else{
            return "";
        }
    }
}
