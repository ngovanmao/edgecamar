/* Author: Ngo Van Mao
 * Created: 2018 April 17
 * Reference code: https://github.com/sussexwearlab/DataLogger
 *
 */
package io.github.ngovanmao.edgecam.collector;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import io.github.ngovanmao.edgecam.SharedPreferencesHelper;
import io.github.ngovanmao.edgecam.log.CustomLogger;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WiFiDataCollector extends AbstractDataCollector {

    private static final String TAG = WiFiDataCollector.class.getSimpleName();
    private CustomLogger mLogger;
    private float mSamplingPeriodUs;
    private Context mContext;
    // The WiFi manager reference
    private WifiManager mWifiManager = null;

    // Receiver class for monitoring changes in WiFi states
    private WiFiInfoReceiver mWiFiInfoReceiver;

    // Timer to manage specific sampling rates
    private Handler mTimerHandler = null;
    private Runnable mTimerRunnable = null;
    private ArrayList<String> mListAuthorizedWiFiSSID = new ArrayList<String>();
    private ArrayList<ScanResult> mAuthorizedNearbyAPs = new ArrayList<ScanResult>();
    private WifiInfo currentWifiInfo;


    public WiFiDataCollector (Context context, String sessionName, String sensorName,
                              float samplingPeriodUs, int logFileMaxSize) {
        mSensorName = sensorName;
        String path = sessionName + File.separator + mSensorName + "_" + sessionName;

        mLogger = new CustomLogger(context, path, sessionName, mSensorName,
                "txt", false, logFileMaxSize);

        mSamplingPeriodUs = samplingPeriodUs;
        mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        mContext = context;

        if (mSamplingPeriodUs > 0) {
            mTimerHandler = new Handler();
            mTimerRunnable = new Runnable() {
                @Override
                public void run() {
                    logWifiInfo(getScanList());
                    int millis = (int) ((float)1000 / mSamplingPeriodUs);
                    mTimerHandler.postDelayed(this, millis);
                }
            };
        } else {
            mWiFiInfoReceiver = new WiFiInfoReceiver();
        }

        List<WifiConfiguration>  listConfigurations = mWifiManager.getConfiguredNetworks();

        for (WifiConfiguration wifConf : listConfigurations) {
            if (wifConf.status ==  WifiConfiguration.Status.CURRENT ||
                    wifConf.status == WifiConfiguration.Status.ENABLED) {
                mListAuthorizedWiFiSSID.add(wifConf.SSID);
            }
        }
        Log.d(TAG, TextUtils.join(":", mListAuthorizedWiFiSSID));

        // Get info of current connected AP
        currentWifiInfo = mWifiManager.getConnectionInfo();
        String macAdd = currentWifiInfo.getMacAddress();
        String bssid = currentWifiInfo.getBSSID();
        String ssid = currentWifiInfo.getSSID();
        int rssi = currentWifiInfo.getRssi();
        Log.d(TAG, "Current AP MacAdd: " + macAdd +" BSSID: " + bssid + " ssid: " + ssid + " rssi: " + String.valueOf(rssi));
    }

    private List<ScanResult> getScanList(){
        synchronized (this) {
            return mWifiManager.getScanResults();
        }
    }

    private void logWifiInfo(List<ScanResult> scanList){
        // System local time in millis
        long currentMillis = (new Date()).getTime();

        // System nanoseconds since boot, including time spent in sleep.
        //long nanoTime = SystemClock.elapsedRealtimeNanos() + mNanosOffset;

        String message = String.format("%s", currentMillis) + ";"
                + scanList.size();
        if (scanList.size() > 0) {
            mAuthorizedNearbyAPs.clear();
        }
        for (ScanResult scan : scanList){

            // The address of the access point.
            String BSSID = scan.BSSID;
            // The network name.
            String SSID = scan.SSID;
            // The detected signal level in dBm, also known as the RSSI.
            int level = scan.level;
            // The primary 20 MHz frequency (in MHz) of the channel over which the client is communicating with the access point.
            int frequency = scan.frequency;
            // Describes the authentication, key management, and encryption schemes supported by the access point.
            String capabilities = scan.capabilities;

            message += ";"
                    + BSSID + ";"
                    + SSID + ";"
                    + level + ";"
                    + frequency + ";"
                    + capabilities;
            // Store nearby APs which are in the known list with authorized key and enable for use.
            if (mListAuthorizedWiFiSSID.contains("\""+SSID+"\"")) {
                //Log.d(TAG, "inside loop SSID match = " + SSID );
                mAuthorizedNearbyAPs.add(scan);
            }
        }

        //Log.i(TAG, message);
        if (mAuthorizedNearbyAPs.size() > 3) {
            // reduce sampling rate to 0.1 Hz, which means sample every 10s.
            //Log.d(TAG, "mAuthorizedNearbyAPs is large" + mAuthorizedNearbyAPs.toString());
            mSamplingPeriodUs = 0.1f;
        } else {
            //Log.d(TAG, "mAuthorizedNearbyAPs is smaller than 3" + mAuthorizedNearbyAPs.toString());
            mSamplingPeriodUs = SharedPreferencesHelper.getSamplingPeriodWiFiInfo(mContext);

        }
        mLogger.log(message);
        mLogger.log(System.lineSeparator());
    }



    private class WiFiInfoReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "WiFiInfoReceiver started!");
            logWifiInfo(getScanList());
            ScanResult strongestAP = getStrongestAP();
            if (currentWifiInfo.getRssi() < strongestAP.level) {
                // Ask the strongest authorized AP for offloading service
                long currentMillis = (new Date()).getTime();
                String message = String.format("%s", currentMillis);
                Log.d(TAG, message + " -- Ask strongest AP -- " + strongestAP.toString());
            }
        }
    }

    public ScanResult getStrongestAP() {
        ScanResult strongestAP = mAuthorizedNearbyAPs.get(0);
        for (ScanResult scan: mAuthorizedNearbyAPs) {
            if (strongestAP.level < scan.level) {
                strongestAP = scan;
            }
        }
        return strongestAP;
    }


    @Override
    public void start() {
        Log.i(TAG, "start:: starting listener for sensor:" + getSensorName());
        if (mWiFiInfoReceiver != null) {
            mContext.registerReceiver(mWiFiInfoReceiver,
                    new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        } else {
            mTimerHandler.postDelayed(mTimerRunnable, 0);
        }
        mLogger.start();
    }

    @Override
    public void stop() {

        Log.i(TAG,"stop:: Stopping listener for sensor " + getSensorName());

        if (mWiFiInfoReceiver != null) {
            mContext.unregisterReceiver(mWiFiInfoReceiver);
        } else {
            mTimerHandler.removeCallbacks(mTimerRunnable);
        }
        mLogger.stop();
    }

    @Override
    public void haltAndRestartLogging() {
        Log.i(TAG, "haltAndRestartLogging");
        mLogger.stop();
        mLogger.resetByteCounter();
        mLogger.start();
    }
}
