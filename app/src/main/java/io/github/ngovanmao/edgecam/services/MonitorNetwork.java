package io.github.ngovanmao.edgecam.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;


import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.github.ngovanmao.edgecam.Constants;
import io.github.ngovanmao.edgecam.R;
import io.github.ngovanmao.edgecam.SharedPreferencesHelper;


public class MonitorNetwork extends Service {
    private static final String TAG = MonitorNetwork.class.getSimpleName();
    private WifiManager mWifiManager = null;
    // Timer to manage specific sampling rates
    private Handler mTimerHandler = null;
    private Runnable mTimerRunnable = null;
    private Handler mSwitchAPHandler = null;
    private Runnable mSwitchAPRunnable = null;
    private WifiInfo currentWifiInfo;
    private float mSamplingPeriodUs;
    private Context mContext;
    private String mSSID;
    private String mBSSID;

    private List<String> mAuthorizedNearbyAPs = new ArrayList<String>();
    private List<WifiConfiguration> mListAuthorizedWiFiConfigs;
    private List<String> mListAuthorizedWiFiSSID = new ArrayList<String>();


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Called onStartCommand");
        if (intent != null) {
            if (intent.hasExtra(Constants.COMMAND_SERVICE_INTENT_KEY)) {
                String message = intent.getStringExtra(Constants.COMMAND_SERVICE_INTENT_KEY);
                Log.i(TAG, "::onStartCommand with message " + message);
            }
        }
        mContext = getApplicationContext();
        updateWifiInfo();

        mListAuthorizedWiFiConfigs = mWifiManager.getConfiguredNetworks();
        for (WifiConfiguration w : mListAuthorizedWiFiConfigs) {
            mListAuthorizedWiFiSSID.add(w.SSID);
        }
        //Log.d(TAG, mListAuthorizedWiFiConfigs.toString());
        Log.d(TAG, mListAuthorizedWiFiSSID.toString());

        mSamplingPeriodUs = 1.0f;
        mTimerHandler = new Handler();
        mTimerRunnable = new Runnable() {
            @Override
            public void run() {
                checkWifiRSSI(getScanList());
                int delayNextTimerHandler = (int) (1000.0f / mSamplingPeriodUs);
                mTimerHandler.postDelayed(this, delayNextTimerHandler);
            }
        };
        mTimerHandler.postDelayed(mTimerRunnable, 0);

        //Register local broadcast for updating setting config
        IntentFilter receivingIntents = new IntentFilter(Constants.CHANGE_MONITOR_SERVICE_ACTION);
        receivingIntents.addAction(Constants.BROADCAST_MQTT_HANDOVER_ACTION);
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mMessageReceiver, receivingIntents);
        return Service.START_STICKY;
    }

    private void updateWifiInfo() {
        mWifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        currentWifiInfo = mWifiManager.getConnectionInfo();
        String bssid = currentWifiInfo.getBSSID();
        if (bssid != null) {
            mBSSID = bssid.replace("\"", "");
        }
        String ssid = currentWifiInfo.getSSID();
        if (ssid != null) {
            mSSID = ssid.replace("\"", "");
            SharedPreferencesHelper.setApInfo(mContext, String.format("%s %s", mSSID, mBSSID));
        }

    }


    /* Example of MQTT message:
       "{"nextSSID":MrKatEdgexx,"nextBSSID":null,"nextPassword":ngovanmao,
       "elapsedTime":2s,"service":{ip:xx, port:xx,...}}"
     */
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Constants.CHANGE_MONITOR_SERVICE_ACTION.equals(intent.getAction())) {
                String change_service_msg = intent.getStringExtra(Constants.CHANGE_MONITOR_SERVICE_ACTION);
                Log.d(TAG, "received broadcase change service message " + change_service_msg);
                if (change_service_msg.equals(getString(R.string.pref_enable_migration_key))) {
                    mSamplingPeriodUs = 1.0f;
                }
            }

            if (Constants.BROADCAST_MQTT_HANDOVER_ACTION.equals(intent.getAction())) {
                String migratedMsg = intent.getStringExtra(Constants.BROADCAST_MQTT_HANDOVER_ACTION);
                Log.d(TAG, "received handover control msg: " + migratedMsg);
                try {
                    JSONObject mqttMessageJson = new JSONObject(migratedMsg);
                    long currentTime = System.currentTimeMillis();
                    final String nextSSID = mqttMessageJson.getString("nextSSID");
                    final String nextBSSID = mqttMessageJson.getString("nextBSSID");
                    final String nextPassword = mqttMessageJson.getString("nextPassword");
                    long elapsedTime = Long.valueOf(mqttMessageJson.getString("elapsedTime")); //in ms
                    mSwitchAPHandler = new Handler();
                    mSwitchAPRunnable = new Runnable() {
                        @Override
                        public void run() {
                            switchToAP(nextSSID, nextBSSID, nextPassword);
                        }
                    };
                    mSwitchAPHandler.postDelayed(mSwitchAPRunnable, elapsedTime);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private void broadcastMonitorMessage(String message) {
        Intent intent = new Intent(Constants.MONITOR_NETWORK_SERVICE_ACTION);
        intent.putExtra(Constants.MONITOR_NETWORK_SERVICE_ACTION, message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void switchToAP(final String nextSSID, final String nextBSSID,
                            final String nextPassword) {
        if (!mWifiManager.isWifiEnabled()){
            mWifiManager.setWifiEnabled(true);
        }
        boolean isAssociatedBefore = false;
        int nextNetworkID = 0;
        for (WifiConfiguration config : mListAuthorizedWiFiConfigs) {
            Log.d(TAG,String.format("config.SSID = %s, nextSSID %s", config.SSID, nextSSID));
            // Add "\"" around nextSSID for matching with format of config.SSID which include quotations.
            // TODO: add BSSID later when we build the WiFi APs with the same SSID.
            if (config.SSID.equals("\""+ nextSSID + "\"")) {
                nextNetworkID = config.networkId;
                isAssociatedBefore = true;
                Log.d(TAG, String.format("Found a known AP with SSID %s, BSSID %s, networkID %d",
                        nextSSID, nextBSSID, nextNetworkID));
                break;
            }
        }

        // In case the next AP is not associated before, we add it directly.
        if (!isAssociatedBefore) {
            WifiConfiguration wifiConfiguration = new WifiConfiguration();
            wifiConfiguration.SSID = String.format("\"%s\"", nextSSID);
            wifiConfiguration.preSharedKey = String.format("\"%s\"", nextPassword);
            nextNetworkID = mWifiManager.addNetwork(wifiConfiguration);
            Log.d(TAG, "Manual add new netID " + String.valueOf(nextNetworkID));
        }

        Log.d(TAG, "disconnect and reconnect to a new AP-ID " + String.valueOf(nextNetworkID));
        //mWifiManager.disconnect();
        mWifiManager.enableNetwork(nextNetworkID, true);
        mWifiManager.reconnect();
        // Update mSSID and mBSSID, not use the given values to ensure getting correct even reconnect failed.
        currentWifiInfo = mWifiManager.getConnectionInfo();
        mBSSID = currentWifiInfo.getBSSID().replace("\"", "");
        mSSID = currentWifiInfo.getSSID().replace("\"", "");
        SharedPreferencesHelper.setApInfo(mContext, String.format("%s %s", mSSID, mBSSID));
        Toast.makeText(mContext, "Switch to a new AP" + mSSID, Toast.LENGTH_SHORT).show();
    }

    private List<ScanResult> getScanList() {
        synchronized (this) {
            return mWifiManager.getScanResults();
        }
    }

    private void checkWifiRSSI(List<ScanResult> scanList) {
        if (!SharedPreferencesHelper.isEnabledMigration(mContext)) {
            mSamplingPeriodUs = 0.01f;
            return;
        }
        // Get info of surrounding AP
        if (scanList.size() > 0) {
            mAuthorizedNearbyAPs.clear();
        }
        //updateWifiInfo();
        //if (mBSSID == null) return;

        for (ScanResult scan : scanList) {
            String SSID = scan.SSID;
            // Store nearby APs which are in the known list with authorized key and enable for use.
            if (mListAuthorizedWiFiSSID.contains("\""+SSID+"\"")) {
                mAuthorizedNearbyAPs.add(new Gson().toJson(scan));
            }
        }

        if (SharedPreferencesHelper.getEnabledServiceName(mContext) != null) {
            String monitorMsg = String.format("{\"end_user\": %s, \"service_name\": %s, " +
                "\"nearbyAP\": %s }", SharedPreferencesHelper.getUserName(mContext),
                SharedPreferencesHelper.getEnabledServiceName(mContext),
                mAuthorizedNearbyAPs.toString());
            broadcastMonitorMessage(monitorMsg);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTimerHandler != null) {
            mTimerHandler.removeCallbacks(mTimerRunnable);
        }
        if (mSwitchAPRunnable != null) {
            mSwitchAPHandler.removeCallbacks(mSwitchAPRunnable);
        }
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(mMessageReceiver);
        Log.d(TAG, "onDestroy finished");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}