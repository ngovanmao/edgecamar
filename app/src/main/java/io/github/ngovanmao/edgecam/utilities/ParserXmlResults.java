package io.github.ngovanmao.edgecam.utilities;

import android.graphics.RectF;
import android.util.Log;

import io.github.ngovanmao.edgecam.Classifier.Recognition;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;



public final class ParserXmlResults {
    private static final String TAG = ParserXmlResults.class.getSimpleName();

    /* Keyword information */
    private static final String START_TIME = "startTime[ns]";
    private static final String END_TIME = "endTime[ns]";
    private static final String SENT_SIZE = "sentSize[B]";

    private static final String SCALE_FACTOR = "scaleFactor";
    private static final String RESULTS = "results";

    private static final String GENERAL = "general";
    private static final String INDEX_SERVER = "indexServer";
    private static final String PROCESSING_TIME = "processTime[ms]";
    private static final String TRANSFER_TIME = "transferTime[ms]";

    private static final String OWN_LIST = "list";
    private static final String OBJECT = "object";
    private static final String CONFIDENCE = "confidence";
    private static final String BOUNDINGBOX = "bb";

    /* Parse the designated json message:
    { "general":
      {
      'indexServer':2,
      'processTime':2.75
      },
      'list': [
        {'person':u'LeeHsienLoon',
        'confidence':0.782,
        'bb':[510, 337, 732, 560]},

        {'person':u'Obama',
        'confidence':0.81,
        'bb':[1626, 378, 1947, 699]}
       ]
    }
    */
    public static List<Recognition> getRecognitionResultsFromJson(String predictionJsonString)
            throws JSONException {

        //Log.d(TAG, predictionJsonString);

        JSONObject predictionJson = new JSONObject(predictionJsonString);
        final ArrayList<Recognition> recognitions = new ArrayList<Recognition>();

        if (!predictionJson.has(GENERAL)){
            return null;
        }

        JSONArray jsonPredictionArray = predictionJson.getJSONArray(OWN_LIST);

        JSONObject generalJson = predictionJson.getJSONObject(GENERAL);
        int indexServer = generalJson.getInt(INDEX_SERVER);
        String processingTime = generalJson.getString(PROCESSING_TIME);
        String transferTime = generalJson.getString(TRANSFER_TIME);
        RectF location;
        location = new RectF(0,14,0,16);
        recognitions.add(new Recognition("Index", String.valueOf(indexServer),
                Float.valueOf(processingTime),location));

        for (int i = 0; i < jsonPredictionArray.length(); i++) {
            JSONObject person = jsonPredictionArray.getJSONObject(i);
            String title;
            Float confidence;
            title = person.getString(OBJECT);
            confidence = (float) person.getDouble(CONFIDENCE);
            JSONArray bbArray = person.getJSONArray(BOUNDINGBOX);
            location  = new RectF(bbArray.getInt(0), bbArray.getInt(1),
                    bbArray.getInt(2),bbArray.getInt(3));
            recognitions.add(new Recognition("Prediction ", title, confidence, location));
        }
        Log.d(TAG, recognitions.toString());

        return recognitions;

    }

    /* Parse the designated json message:
    example:{'startTime[ns]':3343450104358948,
    'endTime[ns]':3343450995092333,
    'originSize[B]':2137960,
    'sentSize[B]':5765,
    'scaleFactor':0.5285412262156448,
    'results':{
        'general':{
            'indexServer':4,
            'transferTime':0.0128939151764,
            'processTime':0.850496768951},
        'list':[
            {'confidence': 0.9287397861480713,
            'object': 'person',
            'bb': [291.6037292480469, 47.983123779296875, 495.7917785644531, 234.59036254882812]},
            {'confidence': 0.8900589346885681,
            'object': 'person',
            'bb': [65.2385025024414, 45.43611145019531, 221.1314926147461, 301.0388641357422]},
            {'confidence': 0.7262415289878845,
            'object': 'tie',
            'bb': [128.3307580947876, 130.4545135498047, 148.83992671966553, 225.7955780029297]}
            ]
     }}
    */
    public static List<Recognition> getRecognitionCombinedResultsFromJson(String predictionJsonString)
            throws JSONException {
        //Log.d(TAG, predictionJsonString);
        JSONObject resultFromOFServiceJson = null;
        try {
            resultFromOFServiceJson = new JSONObject(predictionJsonString);
        } catch (JSONException  e) {
            Log.e(TAG, String.format("Wrong input parsing %s, msg: %s",
                    predictionJsonString, e.toString()));
        }
        final ArrayList<Recognition> recognitions = new ArrayList<Recognition>();
        if (resultFromOFServiceJson == null) return null;
        if (!resultFromOFServiceJson.has(RESULTS)){
            return null;
        }
        Double scaleFactor = resultFromOFServiceJson.getDouble(SCALE_FACTOR);

        JSONObject resultFromServerJson = resultFromOFServiceJson.getJSONObject(RESULTS);

        JSONObject generalInforJsonObject = resultFromServerJson.getJSONObject(GENERAL);
        JSONArray jsonPredictionArray = resultFromServerJson.getJSONArray(OWN_LIST);

        int indexServer = generalInforJsonObject.getInt(INDEX_SERVER);
        String processingTime = generalInforJsonObject.getString(PROCESSING_TIME);
        RectF location;
        location = new RectF(0,30,0,30);
        recognitions.add(new Recognition("Index", String.valueOf(indexServer),
                Float.valueOf(processingTime),location));

        for (int i = 0; i < jsonPredictionArray.length(); i++) {
            JSONObject person = jsonPredictionArray.getJSONObject(i);
            String title;
            Float confidence;
            title = person.getString(OBJECT);
            confidence = (float) person.getDouble(CONFIDENCE);
            JSONArray bbArray = person.getJSONArray(BOUNDINGBOX);
            int left   = (int) (bbArray.getDouble(0) / scaleFactor);
            int top    = (int) (bbArray.getDouble(1) / scaleFactor);
            int right  = (int) (bbArray.getDouble(2) / scaleFactor);
            int bottom = (int) (bbArray.getDouble(3) / scaleFactor);
            //int slideWidth = (right -left)/2;
            //int slideHight = (bottom - top)/2;
            //location  = new RectF(left + slideWidth, top + slideHight,
            //        right + slideWidth, bottom + slideHight);
            location = new RectF(left, top, right, bottom);
            recognitions.add(new Recognition("Prediction ", title, confidence, location));
        }
        Log.d(TAG, recognitions.toString());
        return recognitions;
    }

    public static String getServiceMonitoringInfo(String combinedMsg, JSONObject userInfo)
            throws JSONException {
        JSONObject msgJson = null;
        JSONObject serviceMonitoringInfo = userInfo;
        try {
            msgJson = new JSONObject(combinedMsg);
        } catch (JSONException  e) {
            Log.e(TAG, String.format("Cannot parse %s, error: %s",
                    combinedMsg, e.toString()));
        }

        if (msgJson == null || !msgJson.has(RESULTS)){
            return null;
        }
        long startTime = msgJson.getLong(START_TIME);
        long endTime = msgJson.getLong(END_TIME);
        int sentSize = msgJson.getInt(SENT_SIZE);
        JSONObject resultFromServerJson = msgJson.getJSONObject(RESULTS);
        JSONObject generalInforJsonObject = resultFromServerJson.getJSONObject(GENERAL);
        float processingTime = Float.valueOf(generalInforJsonObject.getString(PROCESSING_TIME));

        try {
            serviceMonitoringInfo.put(START_TIME, startTime);
            serviceMonitoringInfo.put(END_TIME, endTime);
            serviceMonitoringInfo.put(SENT_SIZE, sentSize);
            serviceMonitoringInfo.put(PROCESSING_TIME, processingTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return serviceMonitoringInfo.toString();
    }
}
