package io.github.ngovanmao.edgecam;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import io.github.ngovanmao.edgecam.log.DataCollectionSession;
import com.google.gson.Gson;

public final class SharedPreferencesHelper {
    private static final String TAG = SharedPreferencesHelper.class.getSimpleName();

    /**
     * Retrieves the default SharedPreference object used to store or read values in this app.
     */
    public static SharedPreferences getSharedPrefsInstance(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**********************************************************************************************/
    /* Definition of the getter methods for preferences
    /**********************************************************************************************/

    /**
     * Retrieves the String from SharedPreferences with an specific error message for the ui.
     */
    public static String getUiErrorMessage(Context context, int code){
        return getUiErrorsMessagesList(context)[code];
    }

    /**
     * Retrieves the String list from SharedPreferences with the different errors messages for the ui.
     */
    public static String[] getUiErrorsMessagesList(Context context){
        return context.getResources().getStringArray(R.array.ui_errors_messages_snackbar_list);
    }

    /**
     * Retrieves the DataLoggingSession object from SharedPreferences that contains the data of the collecting session.
     */
    public static DataCollectionSession getDataCollectionSessionObject(Context context){
        String value = getSharedPrefsInstance(context).getString(Constants.DATA_COLLECTION_SESSION_OBJECT_KEY, "");
        if (value.equals("")) {
            return null;
        } else {
            return new Gson().fromJson(value, DataCollectionSession.class);
        }
    }

    public static int getLogFilesMaxsize(Context context){
        String value = getSharedPrefsInstance(context).
                getString(context.getResources().getString(R.string.pref_general_key_log_files_maxSize), "0");
        if (!value.equals("")){
            return Integer.parseInt(value);
        } else {
            return 0;
        }
    }

    //SharedPreferences Sensors - Accelerometer
    public static boolean isEnabledAccelerometer(Context context){
        return getSharedPrefsInstance(context)
                .getBoolean(context.getResources().getString(R.string.pref_sensors_key_enabled_accelerometer), true);
    }

    public static int getSamplingPeriodAccelerometer(Context context){
        return Integer.parseInt(getSharedPrefsInstance(context)
                .getString(context.getResources().getString(R.string.pref_sensors_key_samplingPeriod_accelerometer), "0"));
    }

    //SharedPreferences Sensors - Gyroscope
    public static boolean isEnabledGyroscope(Context context){
        return getSharedPrefsInstance(context)
                .getBoolean(context.getResources().getString(R.string.pref_sensors_key_enabled_gyroscope), true);
    }

    public static int getSamplingPeriodGyroscope(Context context){
        return Integer.parseInt(getSharedPrefsInstance(context)
                .getString(context.getResources().getString(R.string.pref_sensors_key_samplingPeriod_gyroscope), "0"));
    }
    //SharedPreferences Sensors - Magnetometer
    public static boolean isEnabledMagnetometer(Context context){
        return getSharedPrefsInstance(context)
                .getBoolean(context.getResources().getString(R.string.pref_sensors_key_enabled_magnetometer), true);
    }

    public static int getSamplingPeriodMagnetometer(Context context){
        return Integer.parseInt(getSharedPrefsInstance(context)
                .getString(context.getResources().getString(R.string.pref_sensors_key_samplingPeriod_magnetometer), "0"));
    }

    //SharedPreferences Sensors - Location
    public static boolean isEnabledLocation(Context context){
        return getSharedPrefsInstance(context)
                .getBoolean(context.getResources().getString(R.string.pref_sensors_key_enabled_location), true);
    }
    /**
     * Modifies the DataLoggingSession object from SharedPreferences that contains the data of the collecting session.
     */
    public static void setDataCollectionSessionObject(Context context, DataCollectionSession dataCollectionSession){
        getSharedPrefsInstance(context).edit()
                .putString(Constants.DATA_COLLECTION_SESSION_OBJECT_KEY,
                        (dataCollectionSession == null) ? "" : new Gson().toJson(dataCollectionSession))
                .commit();
    }


    public static boolean isEnabledMigration(Context context) {
        return getSharedPrefsInstance(context)
                .getBoolean(context.getResources().getString(R.string.pref_enable_migration_key), true);
    }

    public static int getNumberContinuousQueries(Context context) {
        return Integer.parseInt(getSharedPrefsInstance(context)
                .getString(context.getResources().getString(R.string.pref_number_continuous_queries_key), "1"));
    }

    /**
     * Retrieves the String from SharedPreferences that contains the user name.
     */
    public static String getUserName(Context context){
        return getSharedPrefsInstance(context)
                .getString(context.getResources().getString(R.string.pref_general_user_name_key),
                        context.getResources().getString(R.string.pref_general_user_name_default));
    }

    public static void setUserName(Context context, String userName) {
        SharedPreferences sharedPreferences = getSharedPrefsInstance(context);
        SharedPreferences.Editor editor = sharedPreferences.edit().putString(
                context.getResources().getString(R.string.pref_general_user_name_key), userName);
        editor.commit();
    }

    public static String getBrokerIP(Context context) {
        return getSharedPrefsInstance(context)
                .getString(context.getResources().getString(R.string.pref_broker_ip_key),
                        context.getResources().getString(R.string.pref_broker_ip_default));

    }

    public static void setApInfo(Context context, String apInfo) {
        SharedPreferences sharedPreferences = getSharedPrefsInstance(context);
        SharedPreferences.Editor editor = sharedPreferences.edit().putString(context.getResources().getString(R.string.pref_ap_info_key), apInfo);
        editor.commit();
    }

    public static String getApInfo(Context context) {
        return getSharedPrefsInstance(context)
                .getString(context.getResources().getString(R.string.pref_ap_info_key),
                        null);

    }

    public static void setEnabledServiceInfo(Context context, String serviceIP, String servicePort) {
        setServiceInfo(context, getEnabledServiceName(context), serviceIP, servicePort);
    }

    public static void setServiceInfo(Context context, String serviceName, String serviceIP, String servicePort) {
        SharedPreferences sharedPreferences = getSharedPrefsInstance(context);
        SharedPreferences.Editor editor;
        if (serviceName.equals(context.getResources().getString(R.string.pref_service_openface_key))) {
            editor = sharedPreferences.edit().putString(context.getResources().getString(R.string.pref_service_openface_key),  serviceIP + ":" + servicePort);
            editor.commit();
        } else if (serviceName.contains(context.getResources().getString(R.string.pref_service_yolo_key))){
            editor = sharedPreferences.edit().putString(context.getResources().getString(R.string.pref_service_yolo_key),  serviceIP + ":" + servicePort);
            editor.commit();
        }
    }

    public static String getEnabledServiceName(Context context) {
        String service_name = null;
        if (isEnabledServiceOpenface(context)) {
            service_name = context.getResources().getString(R.string.pref_service_openface_key);
        } else if (isEnabledServiceYolo(context)) {
            service_name =  context.getResources().getString(R.string.pref_service_yolo_key);
        }
        return service_name;
    }

    public static String getEnabledServiceInfo(Context context) {
        String serviceInfo = null;
        if (SharedPreferencesHelper.isEnabledServiceOpenface(context)) {
            serviceInfo = SharedPreferencesHelper.getInfoServiceOpenface(context);

        } else if (SharedPreferencesHelper.isEnabledServiceYolo(context)) {
            serviceInfo = SharedPreferencesHelper.getInfoServiceYolo(context);
        }
        return serviceInfo;
    }

    public static String getInfoServiceOpenface(Context context) {
        return getSharedPrefsInstance(context)
                .getString(context.getResources().getString(R.string.pref_service_openface_key),
                        context.getResources().getString(R.string.pref_service_openface_default));
    }

    public static boolean isEnabledServiceOpenface(Context context){
        return getSharedPrefsInstance(context)
                .getBoolean(context.getResources().getString(R.string.pref_service_enabled_openface_key), true);
    }

    public static String getInfoServiceYolo(Context context) {
        return getSharedPrefsInstance(context)
                .getString(context.getResources().getString(R.string.pref_service_yolo_key),
                        context.getResources().getString(R.string.pref_service_yolo_default));
    }

    public static boolean isEnabledServiceYolo(Context context){
        return getSharedPrefsInstance(context)
                .getBoolean(context.getResources().getString(R.string.pref_service_enabled_yolo_key), true);
    }

    // Log files
    public static boolean isEnableLogFiles(Context context) {
        return getSharedPrefsInstance(context)
                .getBoolean(context.getResources().getString(R.string.pref_general_enable_log_file_key), true);
    }

    //SharedPreferences Sensors - WiFi
    public static boolean isEnabledWiFi(Context context){
        return getSharedPrefsInstance(context)
                .getBoolean(context.getResources().getString(R.string.pref_sensors_key_enabled_WiFi), true);
    }

    public static float getSamplingPeriodWiFiInfo(Context context){
        String value = getSharedPrefsInstance(context)
                .getString(context.getResources().getString(R.string.pref_sensors_key_samplingPeriod_WiFi), "1");
        if (!value.equals("")){
            return Float.parseFloat(value); //Integer.parseInt(value);
        } else {
            return 0;
        }
    }



}
