package io.github.ngovanmao.edgecam;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.util.Log;
import android.widget.Toast;



public class SettingsFragment extends PreferenceFragmentCompat implements
        Preference.OnPreferenceChangeListener,
        SharedPreferences.OnSharedPreferenceChangeListener
        {

    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.pref_fragment);

        SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();
        PreferenceScreen prefScreen = getPreferenceScreen();
        int count = prefScreen.getPreferenceCount();

        // Go through all of the preferences, and set up their preference summary.
        for (int i = 0; i < count; i++) {
            Preference p = prefScreen.getPreference(i);
            // You don't need to set up preference summaries for checkbox preferences because
            // they are already set up in xml using summaryOff and summary On
            if (!(p instanceof CheckBoxPreference)) {
                String value = sharedPreferences.getString(p.getKey(), "");
                setPreferenceCategorySummary(p, value);
            }
        }

        Preference editServiceOpenfacePreference = findPreference(getString(R.string.pref_service_openface_key));
        editServiceOpenfacePreference.setOnPreferenceChangeListener(this);
        Preference editServiceYoloPreference = findPreference(getString(R.string.pref_service_yolo_key));
        editServiceYoloPreference.setOnPreferenceChangeListener(this);
        Preference enabledServiceOpenfacePreference = findPreference(getString(R.string.pref_service_enabled_openface_key));
        enabledServiceOpenfacePreference.setOnPreferenceChangeListener(this);
        Preference enabledServiceYoloPreference = findPreference(getString(R.string.pref_service_enabled_yolo_key));
        enabledServiceYoloPreference.setOnPreferenceChangeListener(this);
        Preference enabledMigrationPreference = findPreference(getString(R.string.pref_enable_migration_key));
        enabledMigrationPreference.setOnPreferenceChangeListener(this);

    }

    private void setPreferenceCategorySummary(Preference preference, String value) {
        if (preference instanceof PreferenceCategory) {
            PreferenceCategory cat = (PreferenceCategory) preference;
            for (int i = 0; i< cat.getPreferenceCount(); i++) {
                Preference p = cat.getPreference(i);
                if (p instanceof EditTextPreference) {
                    String pValue = ((EditTextPreference) p).getText();
                    setPreferenceSummary(p, pValue);
                } else if (p instanceof ListPreference) {
                    String pValue = ((ListPreference) p).getValue();
                    setPreferenceSummary(p, pValue);
                }
            }
        }
        if (preference instanceof  EditTextPreference) {
            setPreferenceSummary(preference, value);
        }
    }
    /**
     * Updates the summary for the preference
     *
     * @param preference The preference to be updated
     * @param value      The value that the preference was updated to
     */
    private void setPreferenceSummary(Preference preference, String value) {
        if (preference instanceof ListPreference) {
            // For list preferences, figure out the label of the selected value
            ListPreference listPreference = (ListPreference) preference;
            int prefIndex = listPreference.findIndexOfValue(value);
            if (prefIndex >= 0) {
                // Set the summary to that label
                listPreference.setSummary(listPreference.getEntries()[prefIndex]);
            }
        } else if (preference instanceof EditTextPreference) {
            // For EditTextPreferences, set the summary to the value's simple string representation.
            preference.setSummary(value);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        Toast error = Toast.makeText(getContext(), "Please select a number between 0.1 and 3", Toast.LENGTH_SHORT);
        return true;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference preference = findPreference(key);
        if (null != preference) {
            // Updates the summary for the preference
            if (preference instanceof EditTextPreference || preference instanceof ListPreference) {
                String value = sharedPreferences.getString(preference.getKey(), "");
                setPreferenceSummary(preference, value);
            }
            if (preference instanceof SwitchPreferenceCompat) {
                if (key.equals(getString(R.string.pref_service_enabled_openface_key))) {
                    if (((SwitchPreferenceCompat) preference).isChecked()) {
                        broadcastMessage(Constants.CHANGE_RUNNING_SERVICE_ACTION,
                                getString(R.string.pref_service_openface_key));
                        SwitchPreferenceCompat editYoloPref = (SwitchPreferenceCompat)
                                findPreference(getString(R.string.pref_service_enabled_yolo_key));
                        editYoloPref.setChecked(false);
                    }
                }
                if (key.equals(getString(R.string.pref_service_enabled_yolo_key))) {
                    if (((SwitchPreferenceCompat) preference).isChecked()) {
                        broadcastMessage(Constants.CHANGE_RUNNING_SERVICE_ACTION,
                                getString(R.string.pref_service_yolo_key));
                        SwitchPreferenceCompat editOpenfacePref = (SwitchPreferenceCompat)
                                findPreference(getString(R.string.pref_service_enabled_openface_key));
                        editOpenfacePref.setChecked(false);
                    }
                }
                if (key.equals(getString(R.string.pref_enable_migration_key))) {
                    if (((SwitchPreferenceCompat) preference).isChecked()) {
                        broadcastMessage(Constants.CHANGE_MONITOR_SERVICE_ACTION, key);
                    }
                }
            }
        }
    }

    private void broadcastMessage(String intentKey, String msg) {
        Log.d("Broadcast", msg);
        Intent intent = new Intent(intentKey);
        intent.putExtra(intentKey, msg);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

}
