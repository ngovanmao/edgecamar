package io.github.ngovanmao.edgecam.sync;

import android.content.Context;

import io.github.ngovanmao.edgecam.utilities.UdpClient;

import java.io.IOException;

public class EdgeSyncTask {
    synchronized public static void syncEdgeServer(Context context) {
        try{
            String serverHost = "192.168.0.41";
            String serverPort = "8888";
            String message = "hello world";
            UdpClient clientSocket = new UdpClient(serverHost, Integer.valueOf(serverPort));
            String githubSearchResults = clientSocket.sendUdpMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
