package io.github.ngovanmao.edgecam.utilities;



import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;


public class UdpClient{

    private static final String TAG = UdpClient.class.getSimpleName();
    static  String dstAddress;
    static int dstPort;

    public UdpClient(String addr, int port) {
        dstAddress = addr;
        dstPort = port;

    }

    public static String  sendUdpImage(String imagePath) throws IOException {

        try {
            DatagramSocket socket = new DatagramSocket();
            InetAddress address = InetAddress.getByName(dstAddress);

            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            int bitmapSize = bitmap.getRowBytes() * bitmap.getHeight();
            ByteBuffer byteBuffer =  ByteBuffer.allocate(bitmapSize);
            bitmap.copyPixelsToBuffer(byteBuffer);
            //ByteArrayOutputStream stream = new ByteArrayOutputStream();
            //bitmap.compress(Bitmap.CompressFormat.JPEG, 0, stream);

            // send request
            //byte[] buf = stream.toByteArray();
            byte[] buf = byteBuffer.array();

            Log.d(TAG, "sendUdpImage sent Image ");

            String lengthHeader = ">Q" + buf.length;
            byte[] bufHeader = lengthHeader.getBytes();
            DatagramPacket packetHeader = new DatagramPacket(bufHeader, bufHeader.length, address, dstPort);
            socket.send(packetHeader);
            int remLen = buf.length;
            int sendLen = Math.min(1024, remLen);
            int currentPosition = 0;
            byte[] part;
            DatagramPacket packet;
            while (remLen > 0) {
                part= Arrays.copyOfRange(buf, currentPosition, sendLen);
                packet = new DatagramPacket(part, part.length, address, dstPort);
                socket.send(packet);
                currentPosition += sendLen;
                remLen -= sendLen;
                sendLen = Math.min(remLen, 1024);
            }

            // get response
            packet = new DatagramPacket(buf, buf.length);
            socket.receive(packet);
            String line = new String(packet.getData(), 0, packet.getLength());
            return line;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }



    public static String  sendUdpMessage(String message) throws IOException {


        try {
            DatagramSocket socket = new DatagramSocket();
            InetAddress address = InetAddress.getByName(dstAddress);
            //InetAddress address = InetAddress.getByName("192.168.1.95");
            Log.d(TAG, "sendUdpMessage change port to " + Integer.toString(dstPort));
            Log.d(TAG, "sendUdpMessage change address to 192.168.1.95" );

            //BufferedImage img = ImageIO.read(new File("src/test.jpg"));

            // send request
            byte[] buf = message.getBytes();

            DatagramPacket packet = new DatagramPacket(buf, buf.length, address, dstPort);
            Log.d(TAG, "sendUdpMessage sent message " + message );
            socket.send(packet);

            // get response
            packet = new DatagramPacket(buf, buf.length);

            socket.receive(packet);
            String line = new String(packet.getData(), 0, packet.getLength());
            return line;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}
