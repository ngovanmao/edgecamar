package io.github.ngovanmao.edgecam.collector;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import io.github.ngovanmao.edgecam.log.CustomLogger;

import java.io.File;
import java.util.Date;

public class SensorDataCollector extends AbstractDataCollector implements SensorEventListener{

    private static final String TAG = SensorDataCollector.class.getSimpleName();

    private int mSensorType;

    private CustomLogger logger = null;

    private int mSamplingPeriodUs;

    private SensorManager mSensorManager;

    public SensorDataCollector(Context context, String sessionName, int sensorType,
                               String sensorName, int samplingPeriodUs, int logFileMaxSize)
            throws Exception {

        mSensorType = sensorType;
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = mSensorManager.getDefaultSensor(mSensorType);

        // In case the sensor cannot be locked or does not exist in the device
        if (sensor == null){
            throw new Exception("Sensor type " + sensorType + " can't be locked or does not exist");
        }

        mSensorName = sensorName;

        String path = sessionName + File.separator + mSensorName + "_" + sessionName;

        logger = new CustomLogger(context, path, sessionName, mSensorName, "txt",
                false, logFileMaxSize);
        mSamplingPeriodUs = samplingPeriodUs;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        // Check that we received the proper sensor event
        if (sensorEvent.sensor.getType() == mSensorType) {
            // System local time in millis
            long currentMillis = (new Date()).getTime();
            // Sensor timestamp defines uptime in nanos (nanoseconds since boot)
            long eventTimestamp = sensorEvent.timestamp;

            //        // System nanoseconds since boot, including time spent in sleep.
            //        long nanoTime = SystemClock.elapsedRealtimeNanos();
            //        // Conversion of sensor timestamp in terms of the system local time
            //        long timeInMillis = currentMillis + (eventTimestamp - nanoTime) / 1000000L;

            String message = String.format("%s", currentMillis) + ";"
                    + String.format("%s", eventTimestamp);

            for (float v : sensorEvent.values) {
                message += String.format(";%s", v);
            }

            logger.log(message);
            logger.log(System.lineSeparator());
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public void start() {
        Log.i(TAG, "start:: Starting listener for sensor: "+getSensorName());
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(mSensorType), mSamplingPeriodUs);
        logger.start();
    }

    @Override
    public void stop() {
        Log.i(TAG,"stop:: Stopping listener for sensor "+getSensorName());
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(this);
        }
        logger.stop();
    }

    @Override
    public void haltAndRestartLogging() {
        logger.stop();
        logger.resetByteCounter();
        logger.start();
    }
}
