package io.github.ngovanmao.edgecam.collector;

import android.content.Context;
import android.util.Log;

import io.github.ngovanmao.edgecam.log.CustomLogger;

import java.io.File;

public class NetPerfDataCollector extends  AbstractDataCollector {
    private static final String TAG = NetPerfDataCollector.class.getSimpleName();
    private CustomLogger mLogger;
    private Context mContext;

    public NetPerfDataCollector(Context context, String sessionName, String sensorName,
                                int logFileMaxSize) {
        mSensorName = sensorName;
        String path = sessionName + File.separator + mSensorName;

        mLogger = new CustomLogger(context, path, sessionName, mSensorName,
                "txt", true, logFileMaxSize);
        mContext = context;
    }

    public void writeLog(String logMessage) {
        mLogger.log(logMessage);
    }

    @Override
    public void start() {
        Log.i(TAG, "start:: starting listener for sensor:" + getSensorName());
        mLogger.start();
    }

    @Override
    public void stop() {
        Log.i(TAG,"stop:: Stopping listener for sensor " + getSensorName());
        mLogger.stop();
    }

    @Override
    public void haltAndRestartLogging() {
        Log.i(TAG, "haltAndRestartLogging");
        mLogger.stop();
        mLogger.resetByteCounter();
        mLogger.start();
    }
}
