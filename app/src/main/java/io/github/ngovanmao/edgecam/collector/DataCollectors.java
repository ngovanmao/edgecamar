package io.github.ngovanmao.edgecam.collector;

import android.content.Context;
import android.hardware.Sensor;
import android.util.Log;

import io.github.ngovanmao.edgecam.Constants;
import io.github.ngovanmao.edgecam.SharedPreferencesHelper;

import java.util.ArrayList;

public class DataCollectors {
    private static final String TAG = DataCollectors.class.getSimpleName();

    // Collectors objects array. One collector per sensor.
    private ArrayList<AbstractDataCollector> mCollectors = null;

    private static DataCollectors instance = null;

    private DataCollectors() {
        // Exists only to defeat instantiation.
    }

    public static DataCollectors getInstance() {
        Log.i(TAG, "::getInstance Creating singleton instance from DataCollectors");
        if(instance == null) {
            instance = new DataCollectors();
        }
        return instance;
    }

    public void startDataCollectors(Context context, String sessionName) {

        Log.i(TAG, "::startDataCollectors Starting data collectors");

        // Initialization of collectors objects array
        mCollectors = new ArrayList<>();

        // Measures the acceleration force in m/s2 that is applied to a device on all three physical axes (x, y, and z), including the force of gravity.
        // SensorEvent.values[0]	Acceleration force along the x axis (including gravity).
        // SensorEvent.values[1]	Acceleration force along the y axis (including gravity).
        // SensorEvent.values[2]	Acceleration force along the z axis (including gravity).
        if (SharedPreferencesHelper.isEnabledAccelerometer(context)) {
            try{
                mCollectors.add(new SensorDataCollector(context, sessionName,
                        Sensor.TYPE_ACCELEROMETER,
                        Constants.SENSOR_NAME_ACC,
                        SharedPreferencesHelper.getSamplingPeriodAccelerometer(context),
                        SharedPreferencesHelper.getLogFilesMaxsize(context)));
            } catch (Exception e){
                Log.e(TAG, "Error creating " + Constants.SENSOR_NAME_ACC + " data collector: " + e.getMessage());
            }
        }

        // Measures a device's rate of rotation in rad/s around each of the three physical axes (x, y, and z).
        // SensorEvent.values[0]	Rate of rotation around the x axis.
        // SensorEvent.values[1]	Rate of rotation around the y axis.
        // SensorEvent.values[2]	Rate of rotation around the z axis.
        if (SharedPreferencesHelper.isEnabledGyroscope(context)){
            try{
                mCollectors.add(new SensorDataCollector(context, sessionName,
                        Sensor.TYPE_GYROSCOPE,
                        Constants.SENSOR_NAME_GYR,
                        SharedPreferencesHelper.getSamplingPeriodGyroscope(context),
                        SharedPreferencesHelper.getLogFilesMaxsize(context)));
            } catch (Exception e){
                Log.e(TAG, "Error creating " + Constants.SENSOR_NAME_GYR + " data collector: " + e.getMessage());
            }
        }

        // Measures the ambient geomagnetic field for all three physical axes (x, y, z) in μT
        // SensorEvent.values[0]	Geomagnetic field strength along the x axis.
        // SensorEvent.values[1]	Geomagnetic field strength along the y axis.
        // SensorEvent.values[2]	Geomagnetic field strength along the z axis.
        if (SharedPreferencesHelper.isEnabledMagnetometer(context)){
            try{
                mCollectors.add(new SensorDataCollector(context, sessionName,
                        Sensor.TYPE_MAGNETIC_FIELD,
                        Constants.SENSOR_NAME_MAG,
                        SharedPreferencesHelper.getSamplingPeriodMagnetometer(context),
                        SharedPreferencesHelper.getLogFilesMaxsize(context)));
            } catch (Exception e){
                Log.e(TAG, "Error creating " + Constants.SENSOR_NAME_MAG + " data collector: " + e.getMessage());
            }
        }
        // Measures the status of the WiFi networks
        if (SharedPreferencesHelper.isEnabledWiFi(context)) {
            Log.d(TAG, "Wifi is enabled!!!");
            mCollectors.add(new WiFiDataCollector(context,
                    sessionName,
                    Constants.SENSOR_NAME_WIFI,
                    SharedPreferencesHelper.getSamplingPeriodWiFiInfo(context),
                    SharedPreferencesHelper.getLogFilesMaxsize(context)));
        }

        // Measures geographic location in latitude, longitude and altitude
        if (SharedPreferencesHelper.isEnabledLocation(context)){
            mCollectors.add(new LocationDataCollector(context, sessionName,
                    Constants.SENSOR_NAME_LOC,
                    SharedPreferencesHelper.getLogFilesMaxsize(context)));
        }

        // All data collectors are started
        for (AbstractDataCollector collector : mCollectors) {
            Log.i(TAG, "Calling collector " + collector.getSensorName());
            collector.start();
        }
    }

    public void stopDataCollectors() {
        // Data collectors are closed
        Log.i(TAG, "::stopDataCollectors Stopping "+Integer.toString(mCollectors.size())+" collectors");
        for (AbstractDataCollector collector : mCollectors) {
            collector.stop();
        }
    }

    public void haltAndRestartLogging(){
        if (mCollectors != null) {
            // Data logging is restarted for evey collector
            for (AbstractDataCollector collector : mCollectors) {
                Log.i(TAG, "::haltAndRestartLogging Restarting logging in collector " + collector.getSensorName());
                collector.haltAndRestartLogging();
            }
        }
    }


}
