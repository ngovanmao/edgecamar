package io.github.ngovanmao.edgecam;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import io.github.ngovanmao.edgecam.collector.DataCollectors;
import io.github.ngovanmao.edgecam.log.DataCollectionSession;

import java.util.Calendar;

public class DataCollectionService extends Service {

    private static final String TAG = DataCollectionService.class.getSimpleName();
    private DataCollectors mDataCollectors;
    private DataCollectionSession mDataCollectionSession;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Called onStartCommand");
        if (intent != null) {
            if (intent.hasExtra(Constants.COMMAND_SERVICE_INTENT_KEY)) {
                String message = intent.getStringExtra(Constants.COMMAND_SERVICE_INTENT_KEY);
                Log.i(TAG, "::onStartCommand with message " + message);
            }
            try {
                /*
                DataCollectionSession storedDataCollectionSessionObject = SharedPreferencesHelper.getDataCollectionSessionObject(this);
                if (storedDataCollectionSessionObject == null) {
                    // A new data collection session object is created
                    mDataCollectionSession = new DataCollectionSession(
                            Calendar.getInstance().getTime(),
                            getResources().getString(R.string.session_id_date_dateformat),
                            SharedPreferencesHelper.getUserName(this),
                            System.currentTimeMillis());
                } else {
                    mDataCollectionSession = storedDataCollectionSessionObject;
                }*/
                // A new data collection session object is created
                mDataCollectionSession = new DataCollectionSession(
                        Calendar.getInstance().getTime(),
                        getResources().getString(R.string.session_id_date_dateformat),
                        SharedPreferencesHelper.getUserName(this),
                        System.currentTimeMillis());

                // The session id is generated for the new data collection session
                String sessionName = mDataCollectionSession.getSessionName();
                Log.i(TAG, "::startDataCollection Starting data collection with session name: " + sessionName);

                mDataCollectors = DataCollectors.getInstance();
                try {
                    mDataCollectors.startDataCollectors(this, sessionName);
                } catch (Exception e) {
                    mDataCollectionSession = null;
                    Log.e(TAG, "Error starting data collectors: " + e.getMessage());
                    return Service.START_NOT_STICKY;
                }

                // If there were no errors starting data collection, the data collection object is stored.
                SharedPreferencesHelper.setDataCollectionSessionObject(this, mDataCollectionSession);

            } catch (Exception e) {
                Log.e(TAG, "::onStartCommand Error processing message : " + e.getMessage());
            }
        }
        return Service.START_STICKY;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDataCollectors.stopDataCollectors();
        Log.d(TAG, "onDestroy Service finished");

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
