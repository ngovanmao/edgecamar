package io.github.ngovanmao.edgecam.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

import io.github.ngovanmao.edgecam.SharedPreferencesHelper;

public class TcpClient {
    private static final String TAG = TcpClient.class.getSimpleName();
    private String mServerIP;
    private int mServerPort;
    private Socket mSocket;
    private OutputStream mOutStream;
    private DataOutputStream mDataOutputStream;
    private InputStreamReader mInputStreamReader;
    private BufferedReader mBufferIn;
    private PrintWriter  mBufferOut;
    private Context mCotext;
    private boolean isRunning;

    public TcpClient(String server_addr, int port, Context cotext) throws IOException {
        this.mServerIP = server_addr;
        this.mServerPort = port;
        mCotext = cotext;
        tryConnectTillTimeout(1000*60);
        getOutInStream();
        isRunning = true;
    }

    private void getOutInStream() throws IOException {
        mOutStream = mSocket.getOutputStream();
        mBufferOut = new PrintWriter(mOutStream);
        mDataOutputStream = new DataOutputStream(mOutStream);
        mInputStreamReader = new InputStreamReader(mSocket.getInputStream(), "UTF-8");
        mBufferIn = new BufferedReader(mInputStreamReader);
    }

    private boolean isServerPortOpen() {
        try {
            mSocket  = new Socket();
            mSocket.connect(new InetSocketAddress(mServerIP, mServerPort), 500);
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    private void tryConnectTillTimeout(long timeout)
            throws IOException {
        // timeout: ms
        long start = System.currentTimeMillis();
        boolean isOK = false;
        while (System.currentTimeMillis() - start < timeout) {
            isOK = isServerPortOpen();
            if (isOK) {
                break;
            } else {
                try {
                    String serviceInfo = SharedPreferencesHelper.getEnabledServiceInfo(mCotext);
                    mServerIP = serviceInfo.split(":")[0];
                    mServerPort = Integer.valueOf(serviceInfo.split(":")[1]);
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        if (!isOK){
            throw new IOException(String.format("Failed to connect to server %s:%d",
                    mServerIP, mServerPort));
        }
        Log.d(TAG, String.format("Connect to %s:%d after %d", mServerIP, mServerPort,
                System.currentTimeMillis() - start));
    }

    public void updateServerInfo(String ip, int port) {
        this.mServerIP = ip;
        this.mServerPort = port;
    }

    public String  sendTcpImage(String imagePath) throws IOException {
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
        double scaleFactor = 1.0;
        int originSize = bitmap.getAllocationByteCount();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 10, baos);
        byte buf [] = baos.toByteArray();
        /* Test the quality parameter in comprees bitmap effects:
            before compress: bytecount = 2137960
            quality = 0: after byte count = 12343 (cannot detect any face)
            quality = 10 after byte count = 19647 (results OK for 2 faces)
            quality = 80 after byte count = 42710
         */
        return sendBytesDataWithInfo(buf, originSize, scaleFactor);
    }

    static final int destWidth = 500;

    public String sendTcpScaleImage(String imagePath) throws IOException {
        //Log.d(TAG, "start sendTcpImage with path " + imagePath);
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
        int originSize = bitmap.getAllocationByteCount();
        int originWidth = bitmap.getWidth();
        int originHeight = bitmap.getHeight();
        double scaleFactor = 1.0;
        Bitmap bitmapScale;
        if (originWidth > destWidth) {
            scaleFactor = (double) destWidth/(double)originWidth;
            int destHeight = (int)(originHeight * scaleFactor);
            bitmapScale = Bitmap.createScaledBitmap(bitmap, destWidth, destHeight, false);
            //Log.d(TAG, "scale size[B] = " + bitmapScale.getAllocationByteCount());
        } else {
            bitmapScale = bitmap;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmapScale.compress(Bitmap.CompressFormat.JPEG, 10, baos);
        byte buf [] = baos.toByteArray();
        /* Test the quality parameter in comprees bitmap effects:
            before compress: bytecount = 2137960
            quality = 0: after byte count = 12343 (cannot detect any face)
            quality = 10 after byte count = 19647 (results OK for 2 faces)
            quality = 80 after byte count = 42710
         */
        return sendBytesDataWithInfo(buf, originSize, scaleFactor);
    }

    public String sendBytesData(byte[] buf) throws IOException {
        int originSize = buf.length;
        return sendBytesDataWithInfo(buf, originSize, 1.0);
    }

    public String sendBytesDataWithInfo(byte[] buf, int originSize, double scale)
            throws IOException {
        int sentSize = buf.length;
        if (!mSocket.isConnected()) {
            tryConnectTillTimeout(10000);
            getOutInStream();
        }
        String combinedResult = null;
        try {
            // Create a equivalent pack in python server
            combinedResult = String.format("{\'startTime[ns]\':%d,", System.nanoTime());
            mDataOutputStream.writeInt(buf.length);
            mDataOutputStream.flush();
            mDataOutputStream.write(buf, 0, buf.length);
            mDataOutputStream.flush();
            Log.e(TAG, String.format("C: sent size[B] = %d", sentSize));
            // TODO: This fix is not perfect, since each image is sent in a separate thread and maybe get back result later.
            if (isRunning) {
                String detectedFaceResults = mBufferIn.readLine();
                combinedResult += String.format("\'endTime[ns]\':%d,\'originSize[B]\':%d," +
                                "\'sentSize[B]\':%d,\'scaleFactor\':%s,\'results\':%s}", System.nanoTime(),
                        originSize, sentSize, scale, detectedFaceResults);
                Log.d(TAG, "combinedMsg: " + combinedResult);
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (isRunning) {
                closeTcpClient();
                tryConnectTillTimeout(60000);
                getOutInStream();
            }
        }
        return combinedResult;
    }

    public String  sendTcpMessage(String message) throws IOException {
        Log.d(TAG, "start sendTcpMessage with message " + message);
        try {
            //mBufferOut.write("Hellow world!");
            //mBufferOut.flush();
            mDataOutputStream.writeInt(message.length());
            mDataOutputStream.flush();
            mBufferOut.write(message);
            mBufferOut.flush();

            String detectedFaceResults = mBufferIn.readLine();
            Log.d(TAG, "received message " +detectedFaceResults);

            return detectedFaceResults;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void closeTcpClient() {
        try {
            Log.d(TAG, "close TCP socket");
            mBufferOut.close();
            mBufferIn.close();
            mSocket.close();
        } catch (IOException e) {
            Log.e(TAG, "Close failed " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void forceCloseTcpClient() {
        isRunning = false;
        closeTcpClient();
    }

}
