package io.github.ngovanmao.edgecam.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Trace;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.util.Size;
import android.widget.Toast;

import io.github.ngovanmao.edgecam.Classifier;
import io.github.ngovanmao.edgecam.Constants;
import io.github.ngovanmao.edgecam.R;
import io.github.ngovanmao.edgecam.SharedPreferencesHelper;
import io.github.ngovanmao.edgecam.collector.NetPerfDataCollector;
import io.github.ngovanmao.edgecam.log.DataCollectionSession;
import io.github.ngovanmao.edgecam.utilities.ImageUtils;
import io.github.ngovanmao.edgecam.utilities.ParserXmlResults;
import io.github.ngovanmao.edgecam.utilities.TcpClient;

import junit.framework.Assert;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Calendar;
import java.util.List;

public class EdgeImageListener implements ImageReader.OnImageAvailableListener {
    private static final String TAG = EdgeImageListener.class.getSimpleName();

    private boolean MAINTAIN_ASPECT = true;
    private boolean isComputing = false;
    private Handler handler;
    private Integer mSensorOrientation;
    private int previewWidth = 0;
    private int previewHeight = 0;
    private byte[][] yuvBytes;
    private int[] rgbBytes = null;
    private Bitmap rgbFrameBitmap = null;
    private Bitmap croppedBitmap = null;
    private Integer sensorOrientation;
    private Matrix frameToCropTransform;
    private boolean SAVE_PREVIEW_BITMAP = false;

    private BoxImageView boxImageView;
    private Context mMainContext;
    private long lastAcquiredImageTime = 0;
    private NetPerfDataCollector mNetPerfDataCollector;
    private DataCollectionSession mDataCollectionSession;
    TcpClient mEdgeTcpClient = null;
    private JSONObject mUserInfo = new JSONObject();

    public void initialize(final Context mainContext, final BoxImageView boxImageView,
            final Handler handler, final Size size, final int sensorOrientation) {
        mMainContext = mainContext;
        this.boxImageView = boxImageView;
        this.handler = handler;
        this.previewWidth = size.getWidth();
        this.previewHeight = size.getHeight();
        Log.i(TAG,String.format("Initializing at size %dx%d", previewWidth, previewHeight));
        this.boxImageView.setPreviewSize(previewWidth, previewHeight);
        Log.i(TAG, String.format("Sensor orientation + screen orientation: %d", sensorOrientation));
        this.mSensorOrientation = sensorOrientation;
        rgbBytes = new int[previewWidth * previewHeight];
        rgbFrameBitmap = Bitmap.createBitmap(previewWidth, previewHeight, Bitmap.Config.ARGB_8888);
        croppedBitmap = Bitmap.createBitmap(Constants.IMAGE_INPUT_SIZE, Constants.IMAGE_INPUT_SIZE, Bitmap.Config.ARGB_8888);

        frameToCropTransform = ImageUtils.getTransformationMatrix(previewWidth, previewHeight,
                Constants.IMAGE_INPUT_SIZE, Constants.IMAGE_INPUT_SIZE, mSensorOrientation, MAINTAIN_ASPECT);
        yuvBytes = new byte[3][];

        // TODO: implement save bitmap option.
        // this.SAVE_PREVIEW_BITMAP = SharedPreferences.getSaveBitmap(mainContext);
        String serverIP = "";
        String serverPort = "";
        String serviceInfo = SharedPreferencesHelper.getEnabledServiceInfo(mMainContext);
        if (serviceInfo == null || serviceInfo == "") {
            Log.e(TAG, "No service is discovered");
            Toast.makeText(mainContext, "Failed to discover edge service. Please check setting!", Toast.LENGTH_LONG).show();
        } else {
            serverIP = serviceInfo.split(":")[0];
            serverPort = serviceInfo.split(":")[1];
            try {
                mEdgeTcpClient = new TcpClient(serverIP, Integer.valueOf(serverPort), mMainContext);
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(mainContext, "Failed to connect to edge service. Please check setting!", Toast.LENGTH_LONG).show();
            }
        }

        try {
            DataCollectionSession storedDataCollectionSessionObject =
                    SharedPreferencesHelper.getDataCollectionSessionObject(mainContext);
            if (storedDataCollectionSessionObject == null) {
                // A new data collection session object is created
                mDataCollectionSession = new DataCollectionSession(
                        Calendar.getInstance().getTime(),
                        mainContext.getResources().getString(R.string.session_id_date_dateformat),
                        SharedPreferencesHelper.getUserName(mainContext),
                        System.currentTimeMillis());
            } else {
                mDataCollectionSession = storedDataCollectionSessionObject;
            }
            // The session id is generated for the new data collection session
            String sessionName = mDataCollectionSession.getSessionName();
            Log.i(TAG, "::startDataCollection Starting data collection with session name: " + sessionName);

            try {
                mNetPerfDataCollector = new NetPerfDataCollector(mainContext, sessionName,
                        "NetPerfStream", SharedPreferencesHelper.getLogFilesMaxsize(mainContext));
                mNetPerfDataCollector.start();
            } catch (Exception e) {
                Log.e(TAG, "Error starting NetPerfStream data collector" + e.getMessage());
            }
            // If there were no errors starting data collection, the data collection object is stored.
            SharedPreferencesHelper.setDataCollectionSessionObject(mainContext, mDataCollectionSession);
        } catch (Exception e) {
            Log.e(TAG, "::onStartCommand Error processing message : " + e.getMessage());
        }

        String serviceName = SharedPreferencesHelper.getEnabledServiceName(mMainContext);
        String userName = SharedPreferencesHelper.getUserName(mMainContext);
        String[] ssidInfo = SharedPreferencesHelper.getApInfo(mMainContext).split("\\s+");
        try {
            mUserInfo.put("end_user", userName);
            mUserInfo.put("service_name", serviceName);
            mUserInfo.put("ssid", ssidInfo[0]);
            mUserInfo.put("bssid", ssidInfo[0]);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void drawResizedBitmap(final Bitmap src, final Bitmap dst) {
        Assert.assertEquals(dst.getWidth(), dst.getHeight());
        final float minDim = Math.min(src.getWidth(), src.getHeight());

        final Matrix matrix = new Matrix();

        // We only want the center square out of the original rectangle.
        final float translateX = -Math.max(0, (src.getWidth() - minDim) / 2);
        final float translateY = -Math.max(0, (src.getHeight() - minDim) / 2);
        matrix.preTranslate(translateX, translateY);

        final float scaleFactor = dst.getHeight() / minDim;
        matrix.postScale(scaleFactor, scaleFactor);

        // Rotate around the center if necessary.
        // Nataniel: added rotation because image is rotated on my device (Pixel C tablet)
        // TODO: Find out if this is happenning in every device.
        sensorOrientation = 90;
        if (sensorOrientation != 0) {
            matrix.postTranslate(-dst.getWidth() / 2.0f, -dst.getHeight() / 2.0f);
            matrix.postRotate(sensorOrientation);
            matrix.postTranslate(dst.getWidth() / 2.0f, dst.getHeight() / 2.0f);
        }
        //LOGGER.i("sensorOrientationImageListener" + sensorOrientation.toString());

        final Canvas canvas = new Canvas(dst);
        canvas.drawBitmap(src, matrix, null);
    }

    protected void fillBytes(final Image.Plane[] planes, final byte[][] yuvBytes) {
        // Because of the variable row stride it's not possible to know in
        // advance the actual necessary dimensions of the yuv planes.
        for (int i = 0; i < planes.length; ++i) {
            final ByteBuffer buffer = planes[i].getBuffer();
            if (yuvBytes[i] == null) {
                Log.d(TAG, String.format("Initializing buffer %d at size %d", i, buffer.capacity()));
                yuvBytes[i] = new byte[buffer.capacity()];
            }
            buffer.get(yuvBytes[i]);
        }
    }
    private long lastOnImageAvailable = 0;
    @Override
    public void onImageAvailable(ImageReader imageReader) {
        Image image = null;
        Log.d(TAG, "Delta onImageAvailable " + String.valueOf(SystemClock.elapsedRealtimeNanos() - lastOnImageAvailable));
        lastOnImageAvailable = SystemClock.elapsedRealtimeNanos();

        try {
            image = imageReader.acquireLatestImage();
            if (image == null) return;

            // No mutex needed as this method is not reentrant.
            if (isComputing) {
                image.close();
                if (System.currentTimeMillis() - lastAcquiredImageTime > 50) {
                    Log.d(TAG, "too long for getting result " + String.valueOf(System.currentTimeMillis() - lastAcquiredImageTime));
                    isComputing = false;
                }
                return;
            }
            isComputing = true;
            lastAcquiredImageTime = System.currentTimeMillis();

            //Trace.beginSection("imageAvailable");
            final Image.Plane[] planes = image.getPlanes();
            //Log.d(TAG, String.format("preW=%s, preH=%s, imageW=%s, imageH=%s", previewWidth,previewHeight, image.getWidth(), image.getHeight()));
            fillBytes(planes, yuvBytes);
            /*for (int i = 0; i < planes.length; ++i) {
                yuvBytes[i] = new byte[planes[i].getBuffer().capacity()];
                planes[i].getBuffer().get(yuvBytes[i]);
            } */

            final int yRowStride = planes[0].getRowStride();
            final int uvRowStride = planes[1].getRowStride();
            final int uvPixelStride = planes[1].getPixelStride();
            ImageUtils.convertYUV420ToARGB8888(
                    yuvBytes[0],
                    yuvBytes[1],
                    yuvBytes[2],
                    previewWidth,
                    previewHeight,
                    yRowStride,
                    uvRowStride,
                    uvPixelStride,
                    rgbBytes);

            image.close();
        } catch (final Exception e) {
            if (image != null) {
                image.close();
            }
            Log.e(TAG, "Exception!" + e.getMessage());
            Trace.endSection();
            return;
        }

        rgbFrameBitmap.setPixels(rgbBytes, 0, previewWidth, 0, 0, previewWidth, previewHeight);
        //drawResizedBitmap(rgbFrameBitmap, croppedBitmap);
        new Canvas(croppedBitmap).drawBitmap(rgbFrameBitmap, frameToCropTransform, null);
        //new Canvas(rgbFrameBitmap).drawBitmap(rgbFrameBitmap, frameToCropTransform, null);

        // For examining the actual TF input.
        if (SAVE_PREVIEW_BITMAP) {
            ImageUtils.saveBitmap(mMainContext, rgbFrameBitmap);
        }

        if (handler != null) {
            handler.post(sendReceiveRunnable);
        }
    }

    private Runnable sendReceiveRunnable = new Runnable() {
        @Override
        public void run() {
            String detectedFaceResults = null;
            int originSize = rgbFrameBitmap.getByteCount();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            rgbFrameBitmap.compress(Bitmap.CompressFormat.JPEG, 15, baos);
            byte buf[] = baos.toByteArray();
            try {
                detectedFaceResults = mEdgeTcpClient.sendBytesDataWithInfo(buf, originSize, 1.0);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (detectedFaceResults != null) {
                List<Classifier.Recognition> results = null;
                try {
                    //results = ParserXmlResults.getRecognitionResultsFromJson(detectedFaceResults);
                    // use this API with scaleFactor=1.0
                    results = ParserXmlResults.getRecognitionCombinedResultsFromJson(detectedFaceResults);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                boxImageView.setResults(results);
                broadcastMonitorMessage(detectedFaceResults);
                mNetPerfDataCollector.writeLog(detectedFaceResults);
                mNetPerfDataCollector.writeLog(System.lineSeparator());
            }
            isComputing = false;
        }
    };

    private void broadcastMonitorMessage(String message) {
        String serviceInfoMsg = null;
        try {
            serviceInfoMsg = ParserXmlResults.getServiceMonitoringInfo(message, mUserInfo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (serviceInfoMsg != null) {
            Log.d(TAG, "Sending service status " + serviceInfoMsg);
            Intent intent = new Intent(Constants.MONITOR_SLA_ACTION);
            intent.putExtra(Constants.MONITOR_SLA_ACTION, serviceInfoMsg);
            LocalBroadcastManager.getInstance(mMainContext).sendBroadcast(intent);
        }
    }

    public void closeServices() {
        if (sendReceiveRunnable != null) {
            handler.removeCallbacks(sendReceiveRunnable);
        }
        if (mEdgeTcpClient != null) {
            mEdgeTcpClient.forceCloseTcpClient();
        }
    }

}
