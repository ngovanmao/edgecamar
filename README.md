# Introduction
This is an Android app for making demo of offloading computationally intensive tasks
(e.g., face recognition, object recognition) to [an edge service](https://gitlab.com/ngovanmao/edgeapps) which is 
deployed as a Docker container on a nearby edge server or a cloud server. 

This project is a sibling of the project coordinated container migration and 
base-station handover in mobile edge computing: https://gitlab.com/ngovanmao/edgecomputing

* Demo of object recognition Android app with a Docker container--offloaded services running on DevBox cloud server: https://youtu.be/7AzQ88y7K1M

* Demo of object recognition Android app with a Docker container--offloaded services running on Jetson edge server (layer-2 of hierarchcial edge computing): https://youtu.be/6FETIIdDqe8

**For more details, please read our paper:** [Globecom'20] Mao V. Ngo, Tie Luo, Hieu T. Hoang, and Tony Q.S. Quek, "Coordinated Container Migration and Base Station Handover in Mobile Edge Computing," IEEE Global Communications Conference (GLOBECOM), December 2020.  [PDF](https://arxiv.org/abs/2009.05682), [Video](https://youtu.be/IqsHe43lHaw)

# Citation
Please cite EdgeCamAR in your publications if it helps your research:
```
@proceeding{MaoGlobecom2020,
  title="{Coordinated Container Migration and Base Station Handover in Mobile Edge Computing}",
  author={Mao~V.~Ngo and Tie~Luo and Hieu~T.~Hoang and Tony~Q.~S.~Quek},
  booktitle={Proc. IEEE GLOBECOM},
  pages={},
  year={2020},
  month={Dec.},
  address={Taiwan}
}
```


